/**
 * 随机名字类
 */
class randomName extends eui.Component {

    public constructor() {
        super();
    }
    /**单例 */
    private static instance: randomName;
    public static getInstance(): randomName {
        if (this.instance == null) {
            this.instance = new randomName();
        }
        return this.instance;
    }

    //随机昵称方法
    //读取配置文件完成
    private onComplete(): void {

        var data = RES.getRes("nextMan_json");//获取文本
        this.Name_list = data;//进行赋值

    }

    public Name_list: any = undefined;
    /**获取一个随机名字 */
    public getRandomName(): string {
        if (this.Name_list == undefined) {
            this.onComplete();
        }

        let index = Math.floor(Math.random() * this.Name_list.nicheng.length);
        return this.Name_list.nicheng[index];

    }

}