class resManager {
	private selfUserInfo: any;
	private code: any;
	private constructor() {

	}
	private static instance: resManager;
	public static getInstance(): resManager {
		if (this.instance == null) {
			this.instance = new resManager();
		}
		return this.instance;
	}


	private ress = new Array();

	//用字典来加载相应的资源，如果已有，则不在加载，否则则进行加载，以减少资源重复加载的情况
	public addRes(key) {
		let tmp = this.find(key);
		if (tmp != null) {
			console.log("重复加载" + key);
			return tmp;
		}

		tmp = RES.getRes(key);
		this.add(key, tmp);
		return tmp;
	}

	private add(key, value) {   // 添加字典的键值(key:value)
		this.ress[key] = value;
	}

	public find(key) {         // 根据键(key)查找对应的值(value),返回值value
		return this.ress[key];
	}
	public count() {           // 计算字典中的元素个数
		var n = 0;
		for (var key in Object.keys(this.ress)) {
			++n;
		}
		return n;
	}
	public remove(key) {       // 根据键(key)删除对应的值(value)
		delete this.ress[key];
	}
	public clear() {           // 清空字典内容
		for (var key in this.ress) {
			delete this.ress[key];
		}
	}

}