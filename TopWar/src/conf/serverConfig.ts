/**
 * 服务器配置文件
 */
class serverConfig {
	public constructor() {
	}

	public static config = {

		"毒圈半径最大长度": "",
		"mapMaxRangeLength": 1000,

		//从上到下依次为 隐身，冰，剪刀，喝酒，变色，2分荷叶，5分荷叶，20分荷叶，50分荷叶
		"地图宽度": "",
		"gameWidth": 2500,
		"地图长度": "",
		"gameHeight": 2500,
		"AI数量": "",
		"aiNumbers": 4,
		"调试绘制开关": "",
		"debugDrawIsOpen": true,
		"地图范围": "",
		"boundSize": {
			x1: 20,
			x2: 1100,
			y1: 20,
			y2: 700

		},
		//边界移动速度
		"BoundaryVelocity": 0.02,
		//边界移动到最小的位置
		"minLineX": 130,
		/**最大转速 */
		"maxSpeed": 14,
		/**陀螺的初始大小 */
		"TopInitSize": 100,///laa  200
		/**陀螺动画的初始大小 */
		TopAnimationsize: 0.23,///laa  0.4
		/**我们来确定速度的界限 */
		"speedSize": {
			minSpeed: 0,
			maxSpeed: 20,
			SpeedUp: 0.3,
			/**灰色区域 */
			SpeedGray: 0.14,
			/**黄色区域 */
			SpeedYellow: 0.21,
			/**绿色区域 */
			SpeedGreen: 0.28,
			/**红色区域 */
			SpeedRed: 0.35,
			SpeedDown: 0.03,

			SpeedDownzero:0.2,
			speedToRotation: 1,
			maxSpeedRotateSpeed: 20,
			minSpeedRotateSpeed: 0,
			/***碰撞掉速度值 */
			crashSpeedLose:0.05
		},
		/**速度的档位 */
		"speedRange": {
			max: 4
		},
		/*滑动加速功能*/
		"isslide": true,

		/*界外掉血*///laa
		"BloodLoseOut": 15,
		/**能量值系数 */
		"coeff":0.1,
	}

}