/** */

class gameLogic {
	public constructor() {
	}

	//开始游戏
	private myPlayer;

	//本身的世界位置
	private m_phyWorld: phyWorld;

	private uilayer;


	private myBornPoints = [];
	//1 创建所有玩家
	//2 随机玩家的位置
	//3.游戏开始
	//4.游戏结束，显示结束界面
	public gameStart() {
		this.createAIS();
		this.createMyPlayer();
		this.getMapBornData();
	}

	public getGameScene() {
		return this.uilayer;
	}
	public init(phyworld, uilayer) {
		this.m_phyWorld = phyworld;
		this.uilayer = uilayer;
		this.gameStart();
	}

	/**获取地图的数据 */
	public getMapBornData() {
		let topCounts = this.m_phyWorld.getTops();
		let point;
		for (let i = 0; i < topCounts.length; i++) {
			point = mapConf.map1[i];
			topCounts[i].born(point.x, point.y);
		}

		// 	myBornPoints


	}
	public getMyTop() {
		return this.myPlayer;
	}
	//获取名次
	//显示重新开始按钮
	public gameOver() {
		let level = this.m_phyWorld.getRetainPlayerNum();

		
	}

	public createAIS() {
		for (let i = 0; i < serverConfig.config.aiNumbers; i++) {
			let ai = new TopAi();
			ai.nickName = "ai" + i;
			ai.setGameLogic(this);
			this.m_phyWorld.addObjInWorld(ai);
			this.uilayer.addChild(ai);
			//ai.born(500 + i * 50, 500 + i * 50);
		}
	}

	//创建所有的陀螺的位置
	public bronAlltop() {
		let tops = this.m_phyWorld.getTops();




	}
	public getRocker() {

		return this.uilayer.getRock();
	}

	//创建玩家的单位
	public createMyPlayer() {
		let player = new Top();
		player.name = "player";
		this.m_phyWorld.addObjInWorld(player);
		player.setGameLogic(this);
		this.myPlayer = player;
		//	player.born(500, 500);
		this.uilayer.addChild(player);

	}
	//获取随机的位置
	public getRandomPoint() {

	}
}