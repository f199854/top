/**游戏界面 */
class GameScene extends ViewBase {
	public constructor() {
		super();
		this.skinName = "GameSceneSkin";
		this.once(egret.Event.ADDED_TO_STAGE, this.Init, this);
	}
	//------VIEW--------------------------------
	/**操作面板场景 */
	private panelScene: PanelScene;

	//------LAYER-------------------------------
	/**陀螺层 */
	private topLayer: eui.Component;

	//------DATA--------------------------------
	/**初始化 */
	private game_Timer: egret.Timer;
	/**显示定时器 */
	private view_Timer: egret.Timer;
	/**摇杆定时器 */
	private rocker_Timer: egret.Timer;
	/**2d摄像机 */
	private camera2D: control.Camera2D;
	/*物理世界 */
	private m_phyWorld: phyWorld;
	/*逻辑世界 */
	private m_gameLogic: gameLogic;
	/**我自己的陀螺 */
	public myTop: Top;
	private mBoundMgr: BoundMgr;
	/**是否蓄力 */
	private isSpeedUp: boolean;
	/**狂暴计时 */
	public isstimer: egret.Timer = new egret.Timer(1000, 3);
	/**游戏循环定时器 */
	private moveLoop: egret.Timer = new egret.Timer(1000 / 60, -1);
	/**记录当前时间点 */
	public timeStartInset;
	/**初始化 */
	private Init() {
		this.dataInit();
		//this.CameraInit();
		this.phyWorldInit();
		this.logicInit();
		this.m_phyWorld.init(this.m_gameLogic);
		this.m_gameLogic.init(this.m_phyWorld, this);
		this.myTop = this.m_gameLogic.getMyTop();
		this.myTop.birth(this.myTop.x, this.myTop.y);
		//this.camera2D.setTarget(this.myTop);
		this.BeginTimes();
		this.eventInit();
		this.mBoundMgr = new BoundMgr(this);
		//左边的线
		this.mBoundMgr.AddBound({ x: serverConfig.config.boundSize.x1, y: serverConfig.config.boundSize.y1 }, { x: serverConfig.config.boundSize.x1, y: serverConfig.config.boundSize.y2 }, { x: 0, y: -1 })
		//上边的线
		this.mBoundMgr.AddBound({ x: serverConfig.config.boundSize.x1, y: serverConfig.config.boundSize.y1 }, { x: serverConfig.config.boundSize.x2, y: serverConfig.config.boundSize.y1 }, { x: -1, y: 0 })
		//下边的线
		this.mBoundMgr.AddBound({ x: serverConfig.config.boundSize.x1, y: serverConfig.config.boundSize.y2 }, { x: serverConfig.config.boundSize.x2, y: serverConfig.config.boundSize.y2 }, { x: -1, y: serverConfig.config.boundSize.y2 })
		//右边的线
		this.mBoundMgr.AddBound({ x: serverConfig.config.boundSize.x2, y: serverConfig.config.boundSize.y1 }, { x: serverConfig.config.boundSize.x2, y: serverConfig.config.boundSize.y2 }, { x: serverConfig.config.boundSize.x2, y: -1 })


	}


    /**协助其他物理来判断边界碰撞
	 * 1.为了避免逻辑复杂，
	 * 
	 * 
	 */
	public checkCraskBound(point, radis, crashObj = null) {
		if (this.mBoundMgr == null) {
			console.log("this.mBoundMgr 为空");

		}
		return this.mBoundMgr.checkBound(point, radis, crashObj);
	}
	public getRock() {
		return this.panelScene.getRock();
	}
	private phyWorldInit() {
		this.m_phyWorld = new phyWorld();

	}

	/**获取你点击按钮，是否进入蓄力状态 */
	public getSpeedUpStates() {
		return this.isSpeedUp;
	}
	private logicInit() {
		this.m_gameLogic = new gameLogic();

	}
	/**摄像机初始化 */
	private CameraInit() {
		let cameraRect: egret.Rectangle = new egret.Rectangle(0, 0, Adaptive.instance.curWidth(), Adaptive.instance.curHidth());
		this.camera2D = new control.Camera2D(this, cameraRect);
	}
	/**刪除脫落
	 * 1.将其从指定的物理库里面删除
	 * 2.将其从渲染界面删除
	 * 
	 */
	public removeTop(top) {
		console.log("刪除對象" + top.nickName);
		this.m_phyWorld.removeObj(top);
		this.removeChild(top);
	}
	/**开启定时器 */
	private BeginTimes() {
		//	this.game_Timer = new egret.Timer(1000 / 30, -1);
		this.addEventListener(egret.Event.ENTER_FRAME, this.update, this);

		//	this.moveLoop.addEventListener(egret.TimerEvent.TIMER, this.logicUpdate, this);
		//this.moveLoop.start();

		//	this.rocker_Timer = new egret.Timer(1000 / 10, -1);
		//this.rocker_Timer.addEventListener(egret.TimerEvent.TIMER, this.GetMyTopDir, this);
		//	this.rocker_Timer.start();
		//测试性能
		Util.getInstance().testPP();
	}
	private interl = 0;

	private update() {
		this.viewUpdate()
		this.opinion();
		this.panelScene.showBlood();
		if (this.interl < 5) {
			this.interl++;
		}
		else {
			this.logicUpdate();
			this.interl = 0;
		}
		this.panelScene.statrTimer();
	}
	/**LYH判断玩家停留区域 */
	private opinion() {
		//LYH每帧判断是否在红色区域
		if (this.panelScene.pold != Area.Red) {
			this.timeStartInset = Date.now();
			console.log("当前时间" + this.timeStartInset);
		}
		else {
			//this.OffTimer();
			this.timerinset();
		}
		//LYH每帧判断是否在灰色区域
		if (this.panelScene.pold == Area.Gray) {
			//LYH回复正常下降速度
			serverConfig.config.speedSize.SpeedDown = 0.03;
			this.myTop.resetTopStates();
			//加速按钮显示
			this.panelScene.functionShow();
		}
	
	}
	/**LYH3秒计时 */
	public timerinset() {
		let time = Date.now();
		console.log("每帧时间" + time);
		if (time - this.timeStartInset >= 3000) {
			// this.myTop.rageTimer();
			this.myTop.changeMyState(TopState.speedstate);
			this.panelScene.functionHide();
		}


		//延时3秒调用降速方法
		//egret.setTimeout(() => { this.continue() }, this, 3000);
		//this.isstimer.addEventListener(egret.TimerEvent.TIMER_COMPLETE, this.continue, this);

		//this.isstimer.start();
	}

	
	/**获取PK点击数 */
	public getPKClickNum(): number {///laa注释  没用到
		return this.panelScene.getClickNum();
	}
	/**发送PK命令 */
	public sendPKOrder(rockerState: ShootControlState) {
		this.panelScene.dealPKOrder(rockerState);
	}
	/**逻辑帧的更新 */
	private logicUpdate() {
		this.m_phyWorld.update(this);
	}

	/**进度条显示 */
	public progressBarDisplay(rotateSpeed: number) {
		//progressBarTs.getInstance().setSpeed(rotateSpeed);

		this.panelScene.energyPoninterMove(rotateSpeed);


	}

	/**数据初始化 */
	private CleanTimes() {
		this.game_Timer.removeEventListener(egret.TimerEvent.TIMER, this.logicUpdate, this);
		//this.rocker_Timer.removeEventListener(egret.TimerEvent.TIMER, this.GetMyTopDir, this);
	}
	private GetMyTopDir() {
		return this.panelScene.GetRockerDir();

	}
	/**视频观看奖励 */
	// private watchReward(e: GameEvent) {
	// 	if (e.data.data == 0) {
	// 		if (e.data.type == WATCHTYPE.ADDKNIFE) {
	// 			this.addknife = 5;
	// 			WxApi.getInstance().toast("飞刀+3");
	// 			this.beginplayer.updateCenter(BEGINSTATE.NORMAL);
	// 		}
	// 		else if (e.data.type == WATCHTYPE.TRYPLAY) {
	// 			this.beginplayer.updateCenter(BEGINSTATE.SHAREADD);
	// 			this.clickStart();
	// 		}
	// 	}
	// }
	/**数据初始化 */
	private dataInit() {
		CurrentManage.getInstance().setStage(this);
		this.panelScene = SceneManager.instance.GetPanelSceneObj();
	}

	/**事件初始化 */
	private eventInit() {
		this.parent.stage.addEventListener(egret.Event.ENTER_FRAME, this.update, this);
	}
	/**返回开始界面 */
	private backBeginMain() {
		SceneManager.instance.SitchScene(SceneName.beginScene);
	}
	/**更新 */
	public viewUpdate() {

		//this.camera2D.move();

		this.m_phyWorld.viewUpdate();

	}
	/**LYH自己陀螺加速 */
	public MyTopSpeedUp() {
		this.myTop.SpeedUp(this.panelScene.pold);
	}

	/**自己的陀螺发射 */
	public MyTopShoot(dir: Vector_2) {
		this.myTop.changeMyState(TopState.shoot, null, dir);

	}
	public reset() {

	};
	public destroy() {

	};
	/**获取玩家陀螺 */
	public getTop() {
		if (this.myTop == null) {
			console.log("玩家陀螺为null");
			return null;
		}
		return this.myTop;
	}
	public changeEnegry(value) {
		this.panelScene.getShootControl().changeEnergy(value);
	}

	/**发送是否显示点击动画命令 tbc */
	public isShowClick(flag) {
		this.panelScene.receiveDisposeClick(flag);
	}

}

