/**操作面板场景 */
class PanelScene extends ViewBase {
	public constructor() {
		super();
		super.Adaptive();
		this.skinName = "PanelSceneSkin";
		this.once(egret.Event.ADDED_TO_STAGE, this.Init, this);

	}

	/******DATA******/
	/**拼手速点击数 */
	private spellHandSpeed: number;
	/**点击状态 */
	private clickState: ShootControlState;
	//------VIEW--------------------------------
	/**游戏场景 */
	private gameScene: GameScene;
	/**摇杆 */
	private rocker: roleControl;
	/**发射键 */
	private shootControl: ShootControl;

	//------EUI---------------------------------
	/**返回主界面按钮 */
	private backBtn: eui.Button;
	/**加速按钮 */
	private speedUpBtn: eui.Image;
	/**摇杆中心圈 */
	private hand: eui.Image;
	/**摇杆外围圈 */
	private circle: eui.Image;
	/**摇杆容器 */
	private rockerGroup: eui.Group;
	/**结算界面 */
	private settlement: eui.Group;
	/**结算界面返回按钮 */
	private backBeginScene: eui.Image;
	/**射击按钮容器 */
	private shootGroup: eui.Group;
	/**射击按钮外围 */
	private shootCirle: eui.Image;
	/**技能按钮 */
	private skillBtn: eui.Image;
	/**技能能量条 */
	public skillEnergyBar: eui.Image;
	/**技能能量条背景 */
	public skillEnergyBG: eui.Image;
	//--------能量条---------------------
	/**能量条 */
	public progressBar: eui.ProgressBar;

	//----动画播放对象-------
	private click: egret.MovieClip;

	//滑动功能
	private slide: slideControl;

	/**发射按钮 */
	public TheEjection: eui.Image;
	/**能量条底色 */
	public energyBase: eui.Image;
	/**能量条 */
	public Ebergy: eui.Image;
	/**能量指针 */
	public EnergyPoninter: eui.Image;
	/**血量显示 */
	public Blood: eui.BitmapLabel;
	/**速度 */
	public speed: number;
	/**当前时间 */
	public time;
	/**速度衰减计时器 */
	private SpeedCut: egret.Timer = new egret.Timer(1000, 1);
	/**区域划分调用 */
	public pold;
	/**初始化 */
	private Init() {
		this.dataInit();
		this.rockerInit();
		this.shootControlInit();
		this.eventInit();
		this.SlideInit();
		this.progressBar.value = 10;

	}
	/**发射键初始化 */
	private shootControlInit() {
		this.shootControl = new ShootControl(this.skillBtn, this.shootCirle, this.shootGroup, this.skillEnergyBar, this.skillEnergyBG, this);
		this.shootControl.RockerInit(this.stage);

	}
	public getShootControl() {
		return this.shootControl;
	}
	/**摇杆初始化 */
	private rockerInit() {
		this.rocker = new roleControl(this.hand, this.circle, this.rockerGroup);
		this.rocker.RockerInit(this.stage);

	}
	/**获取摇杆方向 */
	public GetRockerDir() {
		return this.rocker.getPosition();
	}

	/**数据初始化 */
	private dataInit() {
		this.spellHandSpeed = 0;
		this.clickState = ShootControlState.normal;
		this.gameScene = SceneManager.instance.GetGameSceneObj();
		this.click = FrameAnimation.getInstance().GetFrameAnimation("Click_ske_mc", 1000, 400, "Click_ske", -1);
		this.click.visible = false;
		this.addChild(this.click);
	}
	/**处理PK命令 */
	public dealPKOrder(rockerState: ShootControlState) {
		if (this.clickState != rockerState) {
			this.clickState = rockerState;
			if (rockerState == ShootControlState.spellHandSpeed) {
				this.shootGroup.visible = false;
				this.showClick();
			} else if (rockerState == ShootControlState.normal) {
				this.shootGroup.visible = true;
				this.hideClick();
			}
		}


	}

	/**血量显示 */
	public showBlood() {
		this.Blood.text = this.gameScene.myTop.blood + "";


		console.log("血量显示" + this.Blood.text);
	}

	/**获取点击次数 *////后续没有用
	public getClickNum(): number {
		let clickNum = this.spellHandSpeed;
		this.spellHandSpeed = 0;
		console.log("clickNum" + clickNum)
		return clickNum;
	}
	/**陀螺发射 */
	public TopShoot(dir: Vector_2) {
		this.gameScene.MyTopShoot(dir);
		//进行锁杆//laa
		//this.rocker.setLock(true);//laa
		console.log("锁住摇杆");

	}
	/**事件初始化 */
	private eventInit() {
		this.backBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.backBeginMain, this);
		this.speedUpBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.MyTopSpeedUpEvent, this);
	}
	/**自己陀螺加速事件 */
	private MyTopSpeedUpEvent(evt: egret.TouchEvent) {
		this.time = Date.now();
		this.gameScene.myTop.isok = true;
		if (this.clickState == ShootControlState.normal) {
			this.gameScene.myTop.setIsClkSpdUp(true);
			this.gameScene.MyTopSpeedUp();

		} else if (this.clickState == ShootControlState.spellHandSpeed) {
			this.spellHandSpeed++;
		}

	}
	////**laa  加速键1s */
	public statrTimer() {
		var timestat = Date.now();
		if (this.speedUpBtn.visible == true) {
			if (timestat - this.time >= 1000) {
				this.gameScene.myTop.isok = false;
			}
		} else {//若加速键消失，取消1s后减速  laa
			this.gameScene.myTop.isok = false;
		}

	}

	/**返回开始界面 */
	private backBeginMain() {
		SceneManager.instance.SitchScene(SceneName.beginScene);
	}
	/**处理结算 */
	private DealSettlement() {
		this.settlement.visible = true;
	}

	/*获取玩家的摇杆 */
	public getRock() {
		if (this.rocker == null) {
			console.log("获取的摇杆为空");
			return null;
		}


		return this.rocker;
	}

	public reset() {

	};
	public destroy() {

	};
	public clear() {
		this.backBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.backBeginMain, this);
		this.rocker.clearYaogan();
	};

	/**显示点击对象 */
	public showClick() {
		this.click.visible = true;
	}
	/**隐藏点击对象 */
	public hideClick() {
		this.click.visible = false;
	}
	//滑动功能初始化
	private SlideInit() {
		this.slide = new slideControl();
		//this.slide.slideInit(this.stage,this.gameScene);
	}



	/**能量指针移动LYH */
	public energyPoninterMove(rotateSpeed) {
		if (this.EnergyPoninter.x >= 835 && this.EnergyPoninter.x <= 860) {
			this.pold = Area.Gray;
			console.log("在灰色区域")
		}
		if (this.EnergyPoninter.x >= 535 && this.EnergyPoninter.x <= 835) {
			this.pold = Area.Yellow;
			console.log("在黄色区域")

		}
		if (this.EnergyPoninter.x >= 370 && this.EnergyPoninter.x <= 535) {
			this.pold = Area.Green;
			console.log("在绿色区域")

		}
		if (this.EnergyPoninter.x >= 275 && this.EnergyPoninter.x <= 370) {
			this.pold = Area.Red;
			//改变玩家表情

			console.log("在红色区域")

		}
		//LYH能量指针移动方法
		if (rotateSpeed <= serverConfig.config.speedSize.maxSpeedRotateSpeed) {
			this.EnergyPoninter.x = this.Ebergy.x + this.Ebergy.width - rotateSpeed / serverConfig.config.speedSize.maxSpeedRotateSpeed * this.Ebergy.width - 15;
			if (this.EnergyPoninter.x >= 860) {
				this.EnergyPoninter.x = 860;
			}
		}



	}

	/**LYH摇杆，加速按钮隐藏 */
	public functionHide() {
		this.speedUpBtn.visible = false;
		//**laa 关闭点击加速功能*/
		this.gameScene.myTop.setIsClkSpdUp(false);
	}
	/**LYH摇杆，加速按钮显示 */
	public functionShow() {

		this.speedUpBtn.visible = true;
	}

	/**处理是否显示点击动画命令   tbc*/
	public receiveDisposeClick(flag) {
		if (flag == null) {
			console.log("flag is null");
			return;
		}
		if (flag) {
			//显示点击动画  隐藏弹射按钮
			this.showClick();
			this.shootGroup.visible = false;
		} else {
			this.hideClick();
			this.shootGroup.visible = true;
		}
	}


}
/**发射按钮状态 */
enum ShootControlState {
	normal, spellHandSpeed
}
/**LYH能量条颜色区域划分 */
enum Area {
	Gray, Yellow, Green, Red
}

