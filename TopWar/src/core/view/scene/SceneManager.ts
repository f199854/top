/**
 * 场景管理类
 */
class SceneManager {
    /* 设置所有场景所在的舞台(根)*/
    private _stage: egret.DisplayObjectContainer;
    /**场景列表 */
    public sceneItem: any[] = [];
    constructor() {
    }
    /**实例 */
    static sceneManager: SceneManager;
    /**获取实例对象 */
    static get instance() {
        if (!this.sceneManager) {
            this.sceneManager = new SceneManager()
        }
        return this.sceneManager
    }
    /**场景初始化 */
    public sceneInit() {
        let beginScene = new BeginScene();
        this.sceneItem[SceneName.beginScene] = beginScene;
        let gameScene = new GameScene();
        this.sceneItem[SceneName.gameScene] = gameScene;
        let panelScene = new PanelScene();
        this.sceneItem[SceneName.panelScene] = panelScene;
        //this.addStaga();
    }
    /**添加舞台 */
    public addStaga() {
        for (let i = 0; i < this.sceneItem.length; i++) {
            this._stage.addChild(this.sceneItem[i]);
        }
    }
    /**
     * 设置根场景
     * @param s 根场景
     */
    public setStage(s: egret.DisplayObjectContainer) {
        this._stage = s
        this.sceneInit();
    }

    /** 移除其他场景*/
    public removeOther() {
        for (let i = 0; i < this.sceneItem.length; i++) {
            if (this.sceneItem[i].parent) {
                this._stage.removeChild(this.sceneItem[i]);
            }
        }
    }
    /**切换场景 */
    public SitchScene(sceneName: SceneName) {
        this.removeOther();
        this._stage.addChild(this.sceneItem[sceneName]);
        if (sceneName == SceneName.gameScene) {
            this._stage.addChild(this.sceneItem[SceneName.panelScene]);
        }
    }
    /**获取操作面板对象 */
    public GetPanelSceneObj() {
        return this.sceneItem[SceneName.panelScene];
    }
    /**获取场景对象 */
    public GetGameSceneObj() {
        return this.sceneItem[SceneName.gameScene];
    }
}
enum SceneName {
    beginScene, gameScene, panelScene
}