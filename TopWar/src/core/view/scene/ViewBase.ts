/**视图基类 */
class ViewBase extends eui.Component implements IPool {
	public constructor() {
		super();
	}
	/**适配 */
	public Adaptive() {
		Adaptive.instance.setScreenAuto(this);
	}
	public reset() {

	};
	public destroy() {

	};
	public clear() {

	};
}
interface IPool {
	reset(): void;
	destroy(): void;
	clear(): void;
}