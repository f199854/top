/**开始界面 */
class BeginScene extends ViewBase {
	public constructor() {
		super();
		super.Adaptive();
		this.skinName = "BeginSceneSkin";
		this.once(egret.Event.ADDED_TO_STAGE, this.Init, this);
	}
	//------EUI---------------------------------
	/**开始按钮 */
	private startGame: eui.Button;
	
	/**初始化 */
	private Init() {
		this.eventInit();
		this.viewInit();
	}
	/**显示部分初始化 */
	public viewInit() {

	}
	/**事件初始化 */
	private eventInit() {
		this.startGame.addEventListener(egret.TouchEvent.TOUCH_TAP, this.StartGame, this);
	}
	/**开始游戏 */
	private StartGame() {
		SceneManager.instance.SitchScene(SceneName.gameScene);
	}
	public reset() {

	};
	public destroy() {

	};
	public clear() {
		this.startGame.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.StartGame, this);

	};
}