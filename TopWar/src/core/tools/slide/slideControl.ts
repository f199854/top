/***
 * 手指滑动屏幕使陀螺加速
 * 1，滑动时其他按键从屏幕上消失
*/
class slideControl extends egret.DisplayObjectContainer {
	public constructor() {
		super();
	}
	//记录滑动初始位置，对比滑动结束位置
	private offsetX: number;
	private offsetY: number;
	//点击背景
	private bgimage: egret.Bitmap;
	//判断玩家滑动有效：滑动距离>0
	private slidok: boolean = true;

	private stagecontainer: any;//舞台容器
	//游戏场景>加速方法
	private gameScene: GameScene;
	private _rockerGroup: eui.Group;
	private _shotControlGroup: eui.Group;
	private _speedbutn: eui.Image;
	private _touchID: any;
	public slideInit(obj: any, gameScene: GameScene, rockerGroup: eui.Group, shootGroup: eui.Group, speedbuttn: eui.Image): void {

		this.stagecontainer = obj;

		//背景点击监听     	
		this.stagecontainer.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.startMove, this);
		this.stagecontainer.addEventListener(egret.TouchEvent.TOUCH_END, this.stopMove, this);
		this.stagecontainer.addEventListener(egret.TouchEvent.TOUCH_MOVE, this.onMove, this);
		this.gameScene = gameScene;
		this._rockerGroup = rockerGroup;
		this._shotControlGroup = shootGroup;
		this._shotControlGroup.anchorOffsetX = 75;
		this._shotControlGroup.anchorOffsetY = 75;
		this._speedbutn = speedbuttn;
	}
	//手指滑动开始
	private startMove(e: egret.TouchEvent): void {
		//确认功能打开
		if (!serverConfig.config.isslide) {
			this.slidok = false;

		}
		//记录滑动起点
		this._touchID = e.touchPointID;
		//滑动开始位置判断
		this.onMove(e);
		this.offsetX = e.stageX;
		this.offsetY = e.stageY;
		this.stagecontainer.removeEventListener(egret.TouchEvent.TOUCH_BEGIN, this.startMove, this);
	}
	//手指滑动结束
	private stopMove(e: egret.TouchEvent) {
		if (!serverConfig.config.isslide) {
			this.slidok = false;
		}
		//移动起点一致
		if (this._touchID != e.touchPointID) {
			console.log("_touchID不同");
			return;
		}
		//恢复其他按钮显示
		this._rockerGroup.visible = true;
		this._shotControlGroup.visible = true;
		this._speedbutn.visible = true;
		//滑动结束位置判断
		this.onDistance(e);

		//滑动效果文本显示
		if (this.slidok) {
			//滑动距离>0，速度+1
			if (e.stageX - this.offsetX > 0 || e.stageY - this.offsetY > 0) {

				this.gameScene.MyTopSpeedUp();

			}
		}

		this.slidok = true;
		//一个移动未结束前只记录一次移动的起点
		this.stagecontainer.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.startMove, this);
	}

	private onMove(e: egret.TouchEvent): void {
		if (!serverConfig.config.isslide) {
			this.slidok = false;
		}
		if (this._touchID != e.touchPointID) {
			console.log("_touchID不同");
			return;
		}
		this.onDistance(e);
		if (this.slidok) {
			this._rockerGroup.visible = false;
			this._shotControlGroup.visible = false;
			this._speedbutn.visible = false;
			console.log("摇杆消失");
		}

	}

	//手指滑动判断是否有效(滑动未触及按钮)
	//滑动点距离 左右按键   this.slidok = true/false;
	private onDistance(e: egret.TouchEvent) {
		//两点之间的距离
		let dxM: number = e.stageX - this._rockerGroup.x;

		let dyM: number = e.stageY - this._rockerGroup.y;

		//点击点与摇杆距离
		let distanceM: number = Math.sqrt(dxM * dxM + dyM * dyM);
		if (distanceM < this._rockerGroup.width / 2) {
			this.slidok = false;
		}

		let dxS: number = e.stageX - this._speedbutn.x;

		let dyS: number = e.stageY - this._speedbutn.y;

		//点击点与加速点距离
		let distanceS: number = Math.sqrt(dxS * dxS + dyS * dyS);
		if (distanceS < this._speedbutn.width / 2) {
			this.slidok = false;
		}

		let dxShoot: number = e.stageX - this._shotControlGroup.x;

		let dyShoot: number = e.stageY - this._shotControlGroup.y;

		//点击点与发射点距离
		let distanceShot: number = Math.sqrt(dxShoot * dxShoot + dyShoot * dyShoot);
		if (distanceShot < this._shotControlGroup.width / 2) {
			this.slidok = false;
		}

	}
}