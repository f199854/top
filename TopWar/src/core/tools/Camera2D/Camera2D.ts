module control {
	/**
	 * @author vinson
	 * 创建时间：2017-12-18 上午9:36:42
	 * 2D摄像机(适用于物理引擎)
	 * 此类与BasicCamera,LayerCamera最大不同是目标是在外部控制移动
	 */
	export class Camera2D {
		/**场景（背景与目标所在处）*/
		private screen: egret.DisplayObject;
		/**摄像机范围（眼睛可以看到的范围）*/
		private cameraRect: egret.Rectangle;
		/**目标（玩家控制的对象）*/
		private target: egret.DisplayObject;
		/**速度（目标移动的速度）*/
		private vx: number = 0;
		private vy: number = 0;
		/**上一次的目标坐标 */
		private prevX: number = 0;
		private prevY: number = 0;
		/**边境值比例（相对摄像机的）*/
		private leftTop: number = 0.5;
		private rightBottom: number = 0.5;
		/**边境值*/
		private leftBoundary: number;
		private topBoundary: number;
		private rightBoundary: number;
		private bottomBoundary: number;
		/**场景最大范围*/
		private screenWidth: number;
		private screenHeight: number;
		private layers: any[] = new Array;
		/**摄像机本地临时变量 */
		private cameraLocalTem: egret.Point = new egret.Point();
		/**目标锚点
		 * 锚点默认是左上，如果居中设置为（0.5,0.5）*/
		private anchorPoint: egret.Point = new egret.Point(0.5, 0.5);
		constructor(screen: egret.DisplayObject, cameraRect: egret.Rectangle) {
			//this.target = target;
			this.screen = screen;
			this.cameraRect = cameraRect;
			this.setBoundary(this.leftTop, this.rightBottom);
			this.screenWidth = screen.width;
			this.screenHeight = screen.height;

		}
		/**定义边界 ，即定义个相对运动的视窗，
		 * 当操作对象没有达到边界的时候，即视窗内时是对象移动。
		 * 当超过这个视窗的时候，就场景移动*/
		public setBoundary(leftTop: number, rightBottom: number): void {
			this.leftTop = leftTop;
			this.rightBottom = rightBottom;
			this.leftBoundary = this.cameraRect.width * this.leftTop;
			this.topBoundary = this.cameraRect.height * this.leftTop;
			this.rightBoundary = this.cameraRect.width * this.rightBottom;
			this.bottomBoundary = this.cameraRect.height * this.rightBottom;
		}
		/**设置目标*/
		public setTarget(target: egret.DisplayObject): void {
			this.target = target;
			this.prevY = Adaptive.instance.curHidth() / 2;
			this.prevX = Adaptive.instance.curWidth() / 2;
		}
		/**清除目标 */
		public clearTarget() {
			this.target = null;
		}
		/**摄像机移动
		 * 场景updata调用
		 */
		public move(): void {
			if (this.target == null)
				return;
			/**临时位置x */
			let temporaryPointX: number;
			/**临时位置y */
			let temporaryPointY: number;
			this.vx = this.target.x - this.prevX;
			this.vy = this.target.y - this.prevY;
			this.prevX = this.target.x;
			this.prevY = this.target.y;

			if (this.vx == 0 && this.vy == 0) {
				return;
			}
			var local: egret.Point = this.target.parent.localToGlobal(this.target.x, this.target.y, this.cameraLocalTem);
			var targetW: number = this.target.width * this.anchorPoint.x;
			var targetH: number = this.target.height * this.anchorPoint.y;
			//-----x轴-----
			if (this.vx > 0) {
				//目标达到右边界
				if (local.x + targetW > this.rightBoundary) {
					temporaryPointX = this.screen.x -= this.vx;
				}
				//场景已经移到右尽头
				if (temporaryPointX < this.cameraRect.width - this.screenWidth) {
					temporaryPointX = this.cameraRect.width - this.screenWidth;
				}
				if (temporaryPointX != null) {
					this.screen.x = temporaryPointX;
				}
			} else if (this.vx < 0) {
				//目标达到左边界
				if (local.x < this.leftBoundary) {
					temporaryPointX = this.screen.x -= this.vx;
				}
				//场景已经移到左尽头
				if (temporaryPointX > this.cameraRect.x) {
					temporaryPointX = this.cameraRect.x;
				}
				if (temporaryPointX != null) {
					this.screen.x = temporaryPointX;
				}
			}
			//-----y轴-----
			if (this.vy > 0) {
				//目标达到上边界
				if (local.y + targetH > this.bottomBoundary) {
					temporaryPointY = this.screen.y -= this.vy;
				}
				//场景已经移到上尽头
				if (temporaryPointY < this.cameraRect.height - this.screenHeight) {
					temporaryPointY = this.cameraRect.height - this.screenHeight;
				}
				if (temporaryPointY != null) {
					this.screen.y = temporaryPointY;
				}
			} else if (this.vy < 0) {
				//目标达到下边界
				if (local.y < this.topBoundary) {
					temporaryPointY = this.screen.y -= this.vy;
				}
				//场景已经移到下尽头
				if (temporaryPointY > this.cameraRect.y) {
					temporaryPointY = this.cameraRect.y;
				}
				if (temporaryPointY != null) {
					this.screen.y = temporaryPointY;
				}
			}
		}
	}
}