/**帧动画管理类*/
class FrameAnimationManage extends eui.Component {
	/**单例对象 */
	private static instance: FrameAnimationManage;
	/**获取单例对象 */
	public static getInstance(): FrameAnimationManage {
		if (this.instance == null) {
			this.instance = new FrameAnimationManage();
		}
		return this.instance;
	}
	/**构造 */
	public constructor() {
		super();
		this.CreateRes();
	}
	/**动画资源名字 */
	private dragonResString = ["Click_ske"]
	/**动画动作名字 */
	private dragonActionSting = ["Click_ske"];

	/**动画资源 */
	private animationResource = [];
	/**图片资源 */
	private dragonTexture = [];
	// private factory = new egret.MovieClipDataFactory();

	/**创建资源 */
	private CreateRes() {
		if (this.animationResource.length > 0) {
			console.log("请勿重复加载动画资源");
			return;
		}
		for (let i = 0; i < this.dragonResString.length; i++) {
			this.loadDragonRes(this.dragonResString[i]);
		}
		console.log("龙骨资源加载完毕");
	}

    /**加载动画资源
    * @param ResName 资源名
    */
	private loadDragonRes(ResName: string) {
		/**动画资源 */
		var skeletonData = resManager.getInstance().addRes(ResName + "_mc_json");
		if (skeletonData == null) {
			console.log("动画资源加载失败：" + ResName);
			return;
		}

		/**贴图图集资源 */
		var texture = resManager.getInstance().addRes(ResName + "_tex_png");
		if (texture == null) {
			console.log("贴图资源加载失败：" + ResName);
			return;
		}
		this.animationResource.push(skeletonData);
		this.dragonTexture.push(texture);
	}

    /**获取帧动画显示对象
     * @param rank 等级
     */
	public GetDragonAnimation(rank: number, x: number, y: number, object) {
		if (rank == null || x == null || y == null || object == null) {
			console.log("数据传输为空" + rank + " " + x + " " + y + " " + object);
			return;
		}
		rank--;
		var mcFactory: egret.MovieClipDataFactory = new egret.MovieClipDataFactory(this.animationResource[rank], this.dragonTexture[rank]);
		/**获取动画对象 */
		var click = new egret.MovieClip(mcFactory.generateMovieClipData(this.dragonActionSting[rank]));
		click.x = x;
		click.y = y;
		click.gotoAndPlay(1, -1);
		object.addChild(click);
		click.visible = false;
		return click;
	}

}