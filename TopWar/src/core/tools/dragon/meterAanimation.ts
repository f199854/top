/**龙骨动画类 */
class meterAanimation extends eui.Component {
    /**单例对象 */
    private static instance: meterAanimation;
    /**获取单例对象 */
    public static getInstance(): meterAanimation {
        if (this.instance == null) {
            this.instance = new meterAanimation();
        }
        return this.instance;
    }
    /**构造 */
    public constructor() {
        super();
        this.CreateRes();
    }
    /**动画资源名字 */
    private dragonResString = ["grimace", "juese_1_2", "grimace"]
    /**动画骨架名字 */
    private dragonSkeletonString = ["Armature", "MovieClip", "Armature"];
    /**动画动作名字 */
    private dragonActionSting = ["newAnimation", "juese1_2", "newAnimation"];

    /**骨架资源 */
    private dragonSkeletonData = [];
    /**图片配置文件资源 */
    private dragonTextureData = [];
    /**图片资源 */
    private dragonTexture = [];
    private factory = new dragonBones.EgretFactory();
    /**创建资源 */
    private CreateRes() {
        if (this.dragonSkeletonData.length > 0) {
            console.log("请勿重复加载龙骨资源");
            return;
        }
        for (let i = 0; i < this.dragonResString.length; i++) {
            this.loadDragonRes(this.dragonResString[i]);
        }
        console.log("龙骨资源加载完毕");
    }
    /**加载龙骨动画资源
    * @param ResName 资源名
    */
    private loadDragonRes(ResName: string) {
        /**骨架资源 */
        var skeletonData = resManager.getInstance().addRes(ResName + "_ske_json");
        if (skeletonData == null) {
            console.log("骨架资源加载失败：" + ResName);
            return;
        }
        /**贴图资源配置文件 */
        var textureData = resManager.getInstance().addRes(ResName + "_tex_json");
        if (textureData == null) {
            console.log("贴图配置文件加载失败：" + ResName);
            return;
        }
        /**贴图图集资源 */
        var texture = resManager.getInstance().addRes(ResName + "_tex_png");
        if (texture == null) {
            console.log("贴图资源加载失败：" + ResName);
            return;
        }
        this.dragonSkeletonData.push(skeletonData);
        this.dragonTextureData.push(textureData);
        this.dragonTexture.push(texture);
    }
    /**获取龙骨显示对象
     * @param rank 等级
     */
    public GetDragonAnimation(rank: number) {
        /**骨架资源 */
        let skeletonData = this.dragonSkeletonData[rank];
        /**贴图配置文件 */
        let textureData = this.dragonTextureData[rank];
        /**贴图文件 */
        let texture = this.dragonTexture[rank];
        /**临时工厂变量 */
        var factory = this.factory;
        let DragonBonesData = factory.getAllDragonBonesData();
        if (this.dragonResString[rank] in DragonBonesData) {

        } else {
            //绑定骨架信息
            factory.addDragonBonesData(factory.parseDragonBonesData(skeletonData));
            //绑定贴图
            factory.addTextureAtlasData(factory.parseTextureAtlasData(textureData, texture));
        }
        /**动画对象 */
        var armature = factory.buildArmature(this.dragonSkeletonString[rank]);
        armature.animation.timeScale = 2.5;
        /**动画显示对象 */
        var armatureDisplay = armature.getDisplay();
        armatureDisplay.x = 0;
        armatureDisplay.y = 0;
        armatureDisplay.scaleX = armatureDisplay.scaleY = serverConfig.config.TopAnimationsize;
        armature.animation.play(this.dragonActionSting[rank]);
        //添加到时钟
        dragonBones.WorldClock.clock.add(armature);
        return armatureDisplay;
    }
    /**开启时钟 */
    public openClock() {
        this.addEventListener(egret.Event.ENTER_FRAME, function (): void {
            dragonBones.WorldClock.clock.advanceTime(0.005);
        }, this);
    }



}