class TweenManger extends egret.DisplayObjectContainer {
	public constructor() {
		super();
	}

	/**单例对象 */
	private static instance: TweenManger;
	/**获取单例对象 */
	public static getInstance(): TweenManger {
		if (this.instance == null) {
			this.instance = new TweenManger();
		}
		return this.instance;
	}

	/**获取缓动动画对象 */
	public getTweenObject(name, loop) {
		if (name == null || loop == null) {
			console.log("传入数据为空");
			return null;
		}
		var tw = egret.Tween.get(name, { loop: loop })
		return tw;
	}
}