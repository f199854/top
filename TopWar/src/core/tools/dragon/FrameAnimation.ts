class FrameAnimation extends eui.Component {

	/**单例对象 */
	private static instance: FrameAnimation;
	/**获取单例对象 */
	public static getInstance(): FrameAnimation {
		if (this.instance == null) {
			this.instance = new FrameAnimation();
		}
		return this.instance;
	}
	/**构造 */
	public constructor() {
		super();
	}
    /**获取帧动画显示对象
     * @param resString 资源名
	 * @param actionName 动画名
     */
	public GetFrameAnimation(resString: string, x: number, y: number, actionName: string, playTime: number): egret.MovieClip {
		var _mcData = resManager.getInstance().addRes(resString + "_json");
		var _mcTexture = resManager.getInstance().addRes(resString + "_png");
		var mcDataFactory = new egret.MovieClipDataFactory(_mcData, _mcTexture);
		var role: egret.MovieClip = new egret.MovieClip(mcDataFactory.generateMovieClipData(actionName));
		role.anchorOffsetX = role.width / 2;
		role.anchorOffsetY = role.height / 2;
		role.gotoAndPlay(1, playTime);
		role.x = x;
		role.y = y;
		return role;
	}


}