/**适配 */
class Adaptive {
	/* 设置所有场景所在的舞台(根)*/
	private _stage: egret.DisplayObjectContainer;
	/**单例 */
	static instanceObj: Adaptive;
	/**获取实例对象 */
	static get instance() {
		if (!this.instanceObj) {
			this.instanceObj = new Adaptive()
		}
		return this.instanceObj
	}
	/**当前游戏宽度*/
	public curWidth(): number {
		return this._stage.width;
	}
	/**当前游戏高度*/
	public curHidth(): number {
		return this._stage.height;
	}
	/**
	* 设置根场景
	* @param s 根场景
	*/
	public setStage(s: egret.DisplayObjectContainer) {
		this._stage = s;
	}
	/**设置fixedNarrow模式下EXML的分辨率自适应
	 * @param target 目标EXML
	*/
	public setScreenAuto(target: ViewBase) {
		target.left = 0;
		target.right = 0;
		target.top = 0;
		target.bottom = 0;
	}
}