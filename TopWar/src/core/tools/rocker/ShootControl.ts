/**发射按钮*/
class ShootControl extends eui.Component {
	public constructor(Handle: eui.Image, Circle: eui.Image, rockerContainer: egret.DisplayObjectContainer, skillEnergyBar: eui.Image, skillEnergyBG: eui.Image, stage: PanelScene) {
		super();
		this.m_stage = stage;
		this.handle = Handle;
		this.circle = Circle;
		this.circle.visible = false;
		this.circle_position = { x: this.circle.x, y: this.circle.y };
		this.handle_position = { x: this.handle.x, y: this.handle.y }
		this.circle_radius = this.handle.width / 2;
		this.handle_radius = this.circle.width / 2;
		this.rockerContainer = rockerContainer;
		this.islock = true;
		this.isPutSkill = false;
		this.skillEnergyBar = skillEnergyBar;
		this.skillEnergyBG = skillEnergyBG;
		this.init();
	}
	/**舞台 */
	private m_stage: PanelScene;
	/**触摸球 */
	public handle: eui.Image;
	/**圈 */
	private circle: eui.Image;
	/**摇杆容器 */
	private rockerContainer: egret.DisplayObjectContainer;
	/**触摸Id */
	private touchID: any;
	/**圈坐标 */
	public circle_position;
	/**触摸球坐标 */
	public handle_position;
	/**小球半径 */
	private circle_radius: number;
	/**外圈半径 */
	private handle_radius: number;
	private jiaodu: number = 0;//返回角度
	private container: any;//容器
	public pos = new Vector_2();
	/**临时坐标变量 */
	private temPoint: egret.Point = new egret.Point();
	/**是否锁定 */
	private islock: boolean;
	/**是否可以放技能 */
	private isPutSkill: boolean;
	/**是否发射 */
	private isShoot: boolean;
	/**冷却值遮罩 */
	public coolDownMask: egret.Shape;
	/**技能能量条 */
	public skillEnergyBar: eui.Image;
	/**技能能量条背景 */
	public skillEnergyBG: eui.Image;
	/**技能能量条角度 */
	public skillEnergyAngle: number;

	/**换算成我们需要的百分比能量 */
	public perEnergy =3.6;

	public myEnergy = 0;

	public myAngry = 0;


	/**CD显示 */
	public show_cd() {
		let shape = this.coolDownMask;
		shape.graphics.clear();
		shape.graphics.beginFill(0x00ffff, 1);
		shape.graphics.moveTo(0, 0);
		shape.graphics.lineTo(this.skillEnergyBar.width / 2, 0);
		shape.graphics.drawArc(0, 0, this.skillEnergyBar.width / 2, 0 * Math.PI / 180, this.myAngry * Math.PI / 180, false);
		shape.graphics.lineTo(0, 0);
		shape.graphics.endFill();
	}
	/**更改我们的能量值 */
	public changeEnergy(value) {
		this.myEnergy += value
		console.log("能量值"+this.myEnergy);
		if (this.myEnergy < 0)
			this.myEnergy = 0;

		if (this.myEnergy > 100) {
			this.myEnergy = 100;
			this.isPutSkill = true;
			//this.myEnergy=0;
		}
		this.energyConversion();
	}
	/**能量转换 */
	private energyConversion() {
		this.myAngry = this.myEnergy * this.perEnergy;
		this.show_cd();
	}

	/**摇杆初始化 */
	public RockerInit(object: any) {
		this.container = object;
		this.handle.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.HandClick, this);
		this.container.addEventListener(egret.TouchEvent.TOUCH_MOVE, this.MoveHandle, this);
		this.container.addEventListener(egret.TouchEvent.TOUCH_END, this.recoverHandle, this);
	}

	public init() {
		this.skillEnergyAngle = 180;
		this.coolDownMask = new egret.Shape();
		this.rockerContainer.addChild(this.coolDownMask);
		this.coolDownMask.rotation = -90;
		this.coolDownMask.x = this.skillEnergyBar.x;
		this.coolDownMask.y = this.skillEnergyBar.y;
		this.skillEnergyBar.mask = this.coolDownMask;
		this.show_cd();
	}
	/**按钮点击 */
	private HandClick(evt: egret.TouchEvent) {
	
		this.islock = false;
		// if (this.isPutSkill) {
		// 	this.skillEnergyBG.visible = false;
		// 	this.skillEnergyBar.visible = false;
		// }
		if (evt.stageX > Adaptive.instance.curWidth() / 2) {
			this.isShoot = false;
		}

	}

	/**摇杆移动 */
	private MoveHandle(evt: egret.TouchEvent) {
		if (!this.isPutSkill) {
			return;
		}
		if (this.islock) {
			return;
		}
		if (evt.stageX > Adaptive.instance.curWidth() / 2) {
			this.skillEnergyBG.visible = false;
			this.skillEnergyBar.visible = false;
			this.isShoot = true;
			this.circle.visible = true;
			let localPoint = this.rockerContainer.globalToLocal(evt.stageX, evt.stageY, this.temPoint);
			var distanceX = Math.abs(localPoint.x - this.circle_position.x);
			var distanceY = Math.abs(localPoint.y - this.circle_position.y);
			var distance = Math.sqrt(Math.pow(distanceX, 2) + Math.pow(distanceY, 2));

			this.touchID = evt.touchPointID;
			var rotation = this.calculateRotation(this.circle_position, { x: localPoint.x, y: localPoint.y });
			var hudu = rotation * Math.PI / 180;
			if (distance < this.circle_radius) {
				this.handle.x = localPoint.x;
				this.handle.y = localPoint.y;
			} else {
				this.handle.x = this.circle_position.x + Math.sin(hudu) * (this.circle_radius);
				this.handle.y = this.circle_position.y - Math.cos(hudu) * (this.circle_radius);
			}
			this.jiaodu = Math.floor(rotation);
		}
	}
	/** 复原小球的位置*/
	private recoverHandle(evt: egret.TouchEvent) {
		this.islock = true;
		this.skillEnergyBG.visible = true;
		this.skillEnergyBar.visible = true;
		if (evt.touchPointID == this.touchID) {
			if (this.isShoot) {
				this.m_stage.TopShoot(this.getPosition());
				this.isPutSkill = false;
				this.myEnergy = 0;
				this.energyConversion();
			}
			this.circle.visible = false;
			this.handle.x = this.circle_position.x;
			this.handle.y = this.circle_position.y;
			this.touchID = null;
			this.pos.x = this.pos.y = 0;
		}
	}
	/**获得方向角度*/
	public getRotation(): number {
		return this.jiaodu;
	}
	/**获取方向角度 */
	public getPosition() {


		this.pos.x = this.handle.x - this.circle_position.x;
		this.pos.y = this.handle.y - this.circle_position.y;
		return this.pos;
	}
	//计算角度 起点到终点
	private calculateRotation(player_position: any, mouse_position: any): number {
		var rotation: number = 0;
		var offestx: number = Math.abs(mouse_position.x - player_position.x);
		var offesty: number = Math.abs(mouse_position.y - player_position.y);
		var alpha: number = Math.atan(offestx / offesty);
		rotation = alpha * 180 / Math.PI;
		if (mouse_position.x - player_position.x > 0 && mouse_position.y - player_position.y > 0) {
			rotation = 180 - rotation;
		} else if (mouse_position.x - player_position.x > 0 && mouse_position.y - player_position.y < 0) {
			rotation = rotation;
		} else if (mouse_position.x - player_position.x < 0 && mouse_position.y - player_position.y < 0) {
			rotation = 360 - rotation;
		} else if (mouse_position.x - player_position.x < 0 && mouse_position.y - player_position.y > 0) {
			rotation = 180 + rotation;
		}
		//console.log(rotation);
		return rotation;
	}
	public clearYaogan() {//清除监听和对象
		// this.timer.removeEventListener(egret.TimerEvent.TIMER, this.sendPosition, this);
		// this.timer.reset();
		if (this.container != null) {
			this.container.removeEventListener(egret.TouchEvent.TOUCH_BEGIN, this.MoveHandle, this);
			this.container.removeEventListener(egret.TouchEvent.TOUCH_END, this.recoverHandle, this);
			this.container.removeEventListener(egret.TouchEvent.TOUCH_MOVE, this.MoveHandle, this);
		}
		if (this.contains(this.handle) && this.contains(this.circle)) {
			this.removeChild(this.handle);
			this.removeChild(this.circle);
		}
	}
}
