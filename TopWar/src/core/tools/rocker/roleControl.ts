/**摇杆 */
class roleControl extends eui.Component {
	public constructor(Handle: eui.Image, Circle: eui.Image, rockerContainer: egret.DisplayObjectContainer) {
		super();
		this.handle = Handle;
		this.circle = Circle;
		this.circle_position = { x: this.circle.x, y: this.circle.y };
		this.handle_position = { x: this.handle.x, y: this.handle.y }
		this.circle_radius = this.handle.width / 2;
		this.handle_radius = this.circle.width / 2;
		this.rockerContainer = rockerContainer;

	}
	/**触摸球 */
	public handle: eui.Image;
	/**圈 */
	private circle: eui.Image;
	/**摇杆容器 */
	private rockerContainer: egret.DisplayObjectContainer;
	/**触摸Id */
	private touchID: any;
	/**圈坐标 */
	public circle_position;
	/**触摸球坐标 */
	public handle_position;
	/**小球半径 */
	private circle_radius: number;
	/**外圈半径 */
	private handle_radius: number;
	private jiaodu: number = 0;//返回角度
	private container: any;//容器
	public pos = new Vector_2();
	/**临时坐标变量 */
	private temPoint: egret.Point = new egret.Point();
	/**是否锁定 */
	private islock: boolean;
	/**摇杆初始化 */
	public RockerInit(object: any) {
		this.container = object;
		this.container.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.MoveHandle, this);
		this.container.addEventListener(egret.TouchEvent.TOUCH_END, this.recoverHandle, this);
		this.container.addEventListener(egret.TouchEvent.TOUCH_MOVE, this.MoveHandle, this);
	}

	/*在这里解锁和锁定手柄
	唯一可以解锁摇杆的地方，防止
	
	*/
	public setLock(islock) {
		this.islock = islock;
	}
	public getLock() {
		return this.islock;
	}

	/**摇杆移动 */
	private MoveHandle(evt: egret.TouchEvent) {
		if (this.islock) {
			return;
		}
		if (evt.stageX < Adaptive.instance.curWidth() / 2) {
			let localPoint = this.rockerContainer.globalToLocal(evt.stageX, evt.stageY, this.temPoint);
			var distanceX = Math.abs(localPoint.x - this.circle_position.x);
			var distanceY = Math.abs(localPoint.y - this.circle_position.y);
			var distance = Math.sqrt(Math.pow(distanceX, 2) + Math.pow(distanceY, 2));

			this.touchID = evt.touchPointID;
			var rotation = this.calculateRotation(this.circle_position, { x: localPoint.x, y: localPoint.y });
			var hudu = rotation * Math.PI / 180;
			if (distance < this.circle_radius) {
				this.handle.x = localPoint.x;
				this.handle.y = localPoint.y;
			} else {
				this.handle.x = this.circle_position.x + Math.sin(hudu) * (this.circle_radius);
				this.handle.y = this.circle_position.y - Math.cos(hudu) * (this.circle_radius);
			}
			this.jiaodu = Math.floor(rotation);
		}
	}
	/** 复原小球的位置*/
	private recoverHandle(evt: egret.TouchEvent) {
		if (evt.touchPointID == this.touchID) {
			this.handle.x = this.circle_position.x;
			this.handle.y = this.circle_position.y;
			this.touchID = null;
			this.pos.x = this.pos.y = 0;
		}
	}
	/**获得方向角度*/
	public getRotation(): number {
		return this.jiaodu;
	}
	/**获取方向角度 */
	public getPosition() {


		if (this.islock) {
	
			return null;
		}

		this.pos.x = this.handle.x - this.circle_position.x;
		this.pos.y = this.handle.y - this.circle_position.y;

		return this.pos;
	}


	//计算角度 起点到终点
	private calculateRotation(player_position: any, mouse_position: any): number {
		var rotation: number = 0;
		var offestx: number = Math.abs(mouse_position.x - player_position.x);
		var offesty: number = Math.abs(mouse_position.y - player_position.y);
		var alpha: number = Math.atan(offestx / offesty);
		rotation = alpha * 180 / Math.PI;
		if (mouse_position.x - player_position.x > 0 && mouse_position.y - player_position.y > 0) {
			rotation = 180 - rotation;
		} else if (mouse_position.x - player_position.x > 0 && mouse_position.y - player_position.y < 0) {
			rotation = rotation;
		} else if (mouse_position.x - player_position.x < 0 && mouse_position.y - player_position.y < 0) {
			rotation = 360 - rotation;
		} else if (mouse_position.x - player_position.x < 0 && mouse_position.y - player_position.y > 0) {
			rotation = 180 + rotation;
		}
		//console.log(rotation);
		return rotation;
	}
	public clearYaogan() {//清除监听和对象
		// this.timer.removeEventListener(egret.TimerEvent.TIMER, this.sendPosition, this);
		// this.timer.reset();
		if (this.container != null) {
			this.container.removeEventListener(egret.TouchEvent.TOUCH_BEGIN, this.MoveHandle, this);
			this.container.removeEventListener(egret.TouchEvent.TOUCH_END, this.recoverHandle, this);
			this.container.removeEventListener(egret.TouchEvent.TOUCH_MOVE, this.MoveHandle, this);
		}
		if (this.contains(this.handle) && this.contains(this.circle)) {
			this.removeChild(this.handle);
			this.removeChild(this.circle);
		}
	}
}
class Vector_2 {
	public static stage: egret.Stage;
	public x: number = 0;
	public y: number = 0;
	public static normalize(horizontal: number, vertical: number): Vector_2 {
		var unit: Vector_2 = new Vector_2();
		var size: number = Math.sqrt(Math.pow(horizontal, 2) + Math.pow(vertical, 2));
		unit.x = horizontal / size;
		unit.y = vertical / size;
		return unit;
	}
}