/**陀螺基类 */
class TopAi extends basicAI {

	/**VIEW---------------------------------- */
	/**陀螺内（圆） */
	protected top_Inner: eui.Image;
	/**陀螺外（刀刃） */
	protected top_outer: eui.Image;

	/**DATA---------------------------------- */
	/**玩家名字 */
	protected playName: string;
	/**半径 */
	protected radius: number;

	public constructor() {
		super();
		super.init();
	}
	/**陀螺初始化 */
	public init() {
		this.dataInit();
		this.viewInit();
		this.isAI = true;
		this.nickName = "ai";
		this.playName = "猛虎下山"
	}
	/**视图初始化 */
	protected viewInit() {
		//外圈
		let knife = resManager.getInstance().addRes("top_knife2_png");
		this.top_outer = new eui.Image();
		this.top_outer.texture = knife;
		this.top_outer.width = this.top_outer.height = this.radius;
		this.top_outer.anchorOffsetX = this.top_outer.anchorOffsetY = this.top_outer.width / 2;
		this.addChild(this.top_outer);

		//内圈
		let head = resManager.getInstance().addRes("top_head2_png");
		this.top_Inner = new eui.Image();
		this.top_Inner.texture = head;
		this.top_Inner.width = this.top_Inner.height = Math.floor(this.radius / 1.59);
		this.top_Inner.anchorOffsetX = this.top_Inner.anchorOffsetY = this.top_Inner.width / 2;
		this.addChild(this.top_Inner);
	}
	/**数据初始化 */
	protected dataInit() {
		this.name = "";
		this.radius = serverConfig.config.TopInitSize;

	}
	/**显示更新 */
	public viewUpdate() {
		this.move();
		this.rotate();

		this.judgeIsPlayAnimation();
		//設置隨機速度
		// this.setRandomSpeedUp();
		if (this.isTouchSpeedUp)
			this.SpeedUp();
	}

	/**y用于AI產生隨機的速度 */
	protected setRandomSpeedUp() {


		let t = GameUtil.getRandomByWeight([80, 20]);
		console.log("t" + t);
		if (t == 1) {
			//	this.SpeedUp();

			this.isTouchSpeedUp = true;
		}
		else {
			this.isTouchSpeedUp = false;
		}

	}


	/**加速 */
	public SpeedUp() {


		this.rotateSpeed += (serverConfig.config.speedSize.SpeedUp/10);
		if (this.rotateSpeed > serverConfig.config.speedSize.maxSpeedRotateSpeed) {
			this.rotateSpeed = serverConfig.config.speedSize.maxSpeedRotateSpeed;
		}

		console.log(this.rotateSpeed);

	}
	/**移动 */
	protected move() {
		//非空
		if (this.ViewPoint == null) {
			console.log("this.ViewPoint 为 null");
			return null;
		}
		this.ViewPoint.x = this.x;
		this.ViewPoint.y = this.y;
		let speed = this.speed / 6;
		if (this.ViewPoint.equals(this.dataPoint)) {
			return;
		}
		let distance = egret.Point.distance(this.ViewPoint, this.dataPoint);
		if (distance > speed) {
			this.ViewTargetPoint.setTo(this.dataPoint.x - this.x, this.dataPoint.y - this.y);
			this.ViewTargetPoint.normalize(speed);
			this.x += this.ViewTargetPoint.x;
			this.y += this.ViewTargetPoint.y;
		} else {
			this.x = this.dataPoint.x;
			this.y = this.dataPoint.y;
		}
	}
	/**减转速 */
	public lowRotateSpeed() {
		if (this.rotateSpeed <= 0) {
			this.rotateSpeed = 0;
			return;
		}
		this.rotateSpeed -= this.deltaRotateSpeed;
	}
	/**转 */
	public rotate() {
		this.top_outer.rotation += this.rotateSpeed;
	}
	/**弹开 */
	protected flick() {

	}
	/**发射 */
	protected shoot() {

	}
	/**死亡 */
	protected deth() {

	}
	public clear() {

	}
	/**重置 */
	public reset() {
		this.name = "";
		this.radius = 0;
	}
	/**销毁 */
	public destroy() {

	}

	public deathCallBack() {


		this.m_gameLogic.getGameScene().removeTop(this);

	}

}
