
/**基础的AI实现基础的判断 */
class basicAI extends basicTop {
	public constructor() {
		super();
	}

	/**防撞墙权重 */
	protected Obstacle_weight: number;
	/**思考间隔 */
	protected Think_interval = 100;
	/**思考时间 */
	protected Think_time;
	/**思考时间 */
	protected Min_Think_time = 500;
	/**随机移动权重 */
	protected RandomMove_weight = 50;
	/**用来保存场景的指针 */
	private m_gameScene;
	/**用來判斷速度的增加 */
	protected isTouchSpeedUp = false;
	public init() {
		this.isAI = true;
		this.Obstacle_weight = this.myCrashRange * 1.5;
		this.Think_time = egret.getTimer();
	}
	public SpeedUp() {
	}
	/**重写数据帧，来判断自己的方向 */
	public dataUpdate() {
		//非空
		if (this.dataTargetDir == null) {
			console.log("basicAI	this.dataTargetDir  为空");
			return null;
		}


		this.caluateDir();
		this.dataTargetDir.normalize(this.speed);

		//tbc
		// if (this.myStates.state != TopState.bePk) {
		// 	this.caculataMove();
		// }
		this.caculataMove();
		//tbc  
		this.leaveBeCrash();


		this.updateMyState()

		////laa**界外掉血 */
		if (this.blood > 0) {
			if (this.x < serverConfig.config.boundSize.x1 || this.x > serverConfig.config.boundSize.x2 || this.y < serverConfig.config.boundSize.y1 || this.y > serverConfig.config.boundSize.y2) {
				this.changeBloodOut();
			}
		}
	}

	/**更改本人的狀態 */
	public updateMyState() {

		this.updateFace();
		if (this.myStates.state == TopState.normal)
			return;

		let diftime = Date.now() - this.myStates.statesStartTime;

		//更改状态
		if (diftime > this.getStatesTime(this.myStates.state)) {



			this.resetTopStates();

		}
		//tbc
		// else {

		// 	if (this.myStates.state == TopState.bePk) {
		// 		if (this.PKObj != null) {
		// 			//创建一下点击次数
		// 			let kickTimes = Math.random();//this.m_gameLogic.getGameScene().getPKClickNum();
		// 			//判斷一下兩個人是否有人死了
		// 			if (this.PKObj.getPkValue() <= 0) {
		// 				this.PKObj.death();
		// 				this.changeStates(TopState.normal);

		// 				if (!this.isAI || !this.PKObj.isAI) {
		// 					let gamescene = this.m_gameLogic.getGameScene();
		// 					gamescene.sendPKOrder(ShootControlState.normal);

		// 				}
		// 			}

		// 			this.PKObj.changePkValue(-1 * (kickTimes));
		// 		}
		// 	}

		// }


	}



	/**AI数据帧的时候，计算出自己的方向 更改this.dataTargetDir*/
	public caluateDir() {

		var time = egret.getTimer();
		if (time - this.Think_time > this.Think_interval) {
			this.Think_interval = this.Think_interval + Math.floor(Math.random() * this.Think_interval);
			this.dataTargetDir.setTo(0, 0);
			this.choseMind();
			//判断是否碰墙
			this.checkBoundMind();
			this.Think_time = egret.getTimer();
		}

	}



	//作为临时的方向和角度
	private setRandomMoveDirPoint = new egret.Point(0, 0);
	/**随机移动 */
	protected setRandomMoveDir() {
		var moveDir = Util.getInstance().getRandomDir();
		//	moveDir = new egret.Point(moveDir.x, moveDir.y);
		this.setRandomMoveDirPoint.setTo(moveDir.x, moveDir.y);
		let randomLust = Math.random();
		this.setRandomMoveDirPoint.normalize(this.RandomMove_weight * randomLust);
		this.dataTargetDir.offset(this.setRandomMoveDirPoint.x, this.setRandomMoveDirPoint.y);

	}


	//移动倾向，不会改变一直存在
	public moveTendency = 80;

	//血量越少击杀欲望越弱，技能满击杀欲望提高，如果血量是所有玩家中最少，击杀需求越少
	public killTendency = {
		min: 30,
		nax: 80
	};


	//根据比重来判断AI的行为
	//如果移动属性高，则高纪录移动
	//如果杀人属性高，则优先执行寻搜敌人
	//如果贪婪属性高，则优先找寻道具
	//目前先制作简单的移动，后面进行补全
	public choseMind() {

		let findMind = GameUtil.getRandomByWeight([10, 50, 20]);

		this.isTouchSpeedUp = false;
		if (findMind == 0) {

		}
		else if (findMind == 1) {
			this.mindMove();
		}
		else if (findMind == 2) {
			//	this.mindSpeedUp();
			this.isTouchSpeedUp = true;
		}
	}
	/**决定加速 */
	public mindSpeedUp() {
		this.SpeedUp();
	}
	/**判斷一下移動 */
	public mindMove() {
		this.setRandomMoveDir()
	}
	//检测一下道具,想最近的道具移动
	public DetectionProps() {

	}
	//检测一下敌人,想最近的道具移动
	public DetectionEnemy() {

	}

	//判断是否有人撞过来，有的话则根据随机生成的反应速度，来决定是否躲闪，优先度最高
	public DetectionCrash() {

	}

	/**上一次改变方向的时间 */
	private lasttime: number;
	/**改变方向的时间 */
	private changetime: number;

	private calculateTargetPointTmp = new egret.Point(0, 0);
	// /**判断下次位置是否撞墙 */
	protected checkBoundMind() {
		var nextPoint = Util.getInstance().calculateNextPointEx(this.dataPoint.x, this.dataPoint.y, this.dataTargetDir.x, this.dataTargetDir.y, this.speed, false);

		//如果下次移动会撞墙，则直接反向移动
		if (this.checkBound(nextPoint, this)) {
			console.log("反向移动");
			this.calculateTargetPointTmp.setTo(-this.dataTargetDir.x, -this.dataTargetDir.y);
			this.calculateTargetPointTmp.normalize(this.Obstacle_weight + this.speed);
			this.dataTargetDir.offset(this.calculateTargetPointTmp.x, this.calculateTargetPointTmp.y);
		}

	}








}