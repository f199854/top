class BasicObj extends eui.Component implements IPool {
	public constructor() {
		super();
		this.init();
	}

	protected id;

	//记录自己的坐标点(仅用于数据跟显示无关)
	protected dataPoint = new egret.Point(0, 0);
	protected dataTargetDir = new egret.Point(0, 0);

	/**移速 */
	protected speed = serverConfig.config.speedSize.minSpeed;

	/**转速 */
	protected rotateSpeed = serverConfig.config.speedSize.minSpeedRotateSpeed;


	//用于显示用的坐标点（仅用于显示跟数据无关）
	/**当前点 */
	protected ViewPoint = new egret.Point(0, 0);
	/**目标点 */
	protected ViewTargetPoint = new egret.Point(0, 0);
	//移动方向
	protected targetDir = new egret.Point(0, 0);


	/**视野范围 */
	protected myCrashRange = 30;///laa  50
	/**是否是AI */
	public isAI;
	/**状态 */
	//protected states: TopState = TopState.normal;

	// protected nickName;

	protected lastCrashObj;
	protected m_gameLogic: gameLogic;

	protected m_objType = objType.top;

	public getObjType() {
		return this.m_objType;
	}

	//获取陀螺自身的状态
	public getMyType() {
		return this.m_objType;
	}

	//保存姓名
	public nickName = "basicobj";

	public settopId(id) {
		this.id = id;
	}
	public gettopId() {
		return this.id;
	}
	public setGameLogic(tt) {

		this.m_gameLogic = tt;

	}


	/**獲取 */
	public getDataPoint(others = null) {
		return this.dataPoint;
	}

	//只调用一次
	public born(x, y) {
		this.x = x;
		this.y = y;
		this.dataPoint.setTo(x, y);
		this.ViewPoint.setTo(x, y);


	}
	public getMyCrashRange() {
		return this.myCrashRange;
	}
	public init() {
		this.myCrashRange = 50;

	}

	//设置自己的ID需求
	public setTopId(topid) {
		this.id = topid;
	}
	//清空所有的信息
	public reset() {
		this.id = null;
	}
	public isDeath() {

	}
	/**用于计算自己下个逻辑位置 */
	public dataUpdate() {


		// if (this.states != TopState.normal)
		// 	return;
		// this.dataTargetDir.normalize(this.speed);

		// this.dataPoint.x += this.dataTargetDir.x;
		// this.dataPoint.y += this.dataTargetDir.y;

		// this.updateMyState();
		//console.log("x="+this.dataPoint.x+"y="+this.dataPoint.y);
		// this.lowSpeed();
		// this.lowRotateSpeed();

	}
	protected updateMyState() {

	}
	public lowSpeed() {
		// return;
		// if (this.speed <= 0) {
		// 	this.speed = 0;
		// 	return;
		// }
		// this.speed -= this.deltaSpeed;
	}
	public lowRotateSpeed() {
		// if (this.rotateSpeed <= 0) {
		// 	this.rotateSpeed = 0;
		// 	return;
		// }
		// this.rotateSpeed -= this.deltaRotateSpeed;
	}
	public getRotateSpeed(): number {
		return this.rotateSpeed;
	}
	protected deltaRotateSpeed = 0.01;
	public deltaSpeed = 0.1;
	/**用于顯示自己下个逻辑位置 */
	public viewUpdate() {

	}


	//发生了碰撞
	//1.陀螺，根据两个物体的转速，进行减速计算，更改碰撞状态，计算碰撞后的朝向，
	//2、道具则自我计算
	public beCrash(crashObj) {

		// console.log("碰撞");
		// this.dataTargetDir.setTo(0, 0);
		// let dir = this.calulatePopDir(this.dataPoint, crashObj.getDataPoint());

		// //下一帧的目标点
		// this.dataTargetDir.setTo(dir.x, dir.y);

		// this.states = TopState.beCrash;

	}
	public changeMyState(states) {

	}

	private calulatePointTmp = new egret.Point(0, 0);
	public calulatePopDir(P0, P1) {
		//非空
		if (this.calulatePointTmp == null || P0 == null || P1 == null) {
			console.log("BasicObj this.calulatePointTmp 是null");
			return null;
		}

		this.calulatePointTmp.setTo(P0.x - P1.x, P0.y - P1.y);
		///	let dir0 = P0.subtract(P1);

		//	this.calulatePointTmp.normalize(100);




		return { x: this.calulatePointTmp.x, y: this.calulatePointTmp.y };;
	}


	/**用于死亡
	 * 1.清空数据
	 * 2.播放死亡动画
	 * 
	*/
	public death() {
	//	this.dataPoint = null;
	//	this.dataTargetDir = null;
		this.ViewPoint = null;
		this.ViewTargetPoint = null;
		//移动方向
		this.targetDir = null;

		//將狀態切換為死亡狀態
		this.changeMyState(TopState.death);
		this.deathCallBack();
	}


	/**死亡的时候调用的函数 */
	public deathCallBack() {

	}
	public clear() {

	}
	/**销毁 */
	public destroy() {

	}


}

/**陀螺状态*/
enum objType {
	bound, top, prop
}