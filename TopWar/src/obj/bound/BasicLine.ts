// TypeScript file


//计算一下线的基本属性
class BasicLine {
    public constructor(p1, p2, dataPoint) {

        this.startPoint.setTo(p1.x, p1.y);
        this.endPoint.setTo(p2.x, p2.y);
        this.dataPoint.setTo(dataPoint.x, dataPoint.y);

    }

    protected myStates =
    {
        state: 0,
        lastState: 0,
        statesStartTime: 0,

    };
    public nickName = "bound";
    //起始点
    public startPoint = new egret.Point();
    //结束点
    public endPoint = new egret.Point();
    public setLine(x1, y1, x2, y2) {
        this.startPoint.setTo(x1, y1);
        this.endPoint.setTo(x1, y1);
    }

    public dataPoint = new egret.Point();
    public tmpPoint = new egret.Point();
    public rotateSpeed ;
    /**发生碰撞 */
    public beCrash(crashObjt: BasicObj) {

        if (crashObjt.getObjType() == objType.top) {
            console.log("发生碰撞");
            crashObjt.beCrash(this);

            let points = this.getTwoDataPoint(crashObjt);
            //    CurrentManage.getInstance().DrawPowerGrid(points.point1, points.point2, points.point3);

        }
    }
    public tmpDataPointOne = new egret.Point();
    public tmpDataPointTwo = new egret.Point();
    public tmpDataPointThree = new egret.Point();
    public getDataPoint(crashObj) {
        this.tmpPoint.setTo(this.dataPoint.x, this.dataPoint.y);
        if (this.tmpPoint.x == -1) {
            this.tmpPoint.x = crashObj.x;
        }
        else if (this.tmpPoint.y == -1) {
            this.tmpPoint.y = crashObj.y;
        }
        else {
            console.log("代码显示错");
            return;
        }

        console.log("返回碰撞点" + this.tmpPoint);
        return this.tmpPoint;
    }


    //如果
    public getTwoDataPoint(crashObj) {
        //来获取第一个点的位置
        this.tmpDataPointOne = this.getDataPoint(crashObj);
        //来放置整个点的位置
        this.tmpDataPointTwo.setTo(this.tmpDataPointOne.x, this.tmpDataPointOne.y);

        this.tmpDataPointThree.setTo(this.tmpDataPointOne.x, this.tmpDataPointOne.y);
        //更改电光的位置
        if (this.dataPoint.x == -1) {

            this.tmpDataPointTwo.x = serverConfig.config.boundSize.x1;

            this.tmpDataPointThree.x = serverConfig.config.boundSize.x2;



        }

        else if (this.dataPoint.y == -1) {

            this.tmpDataPointTwo.y = serverConfig.config.boundSize.y1;

            this.tmpDataPointThree.y = serverConfig.config.boundSize.y2;

        }



        return { point1: this.tmpDataPointOne, point2: this.tmpDataPointTwo, point3: this.tmpDataPointThree };
    }

    //获取距离
    private getPointDistance(p1, p2) {
        //	return Math.sqrt(Math.pow(p2.x - p1.x, 2) + Math.pow(p2.y - p1.y, 2)) - p1.radius - p2.radius;
        return Math.sqrt(Math.pow(p2.x - p1.x, 2) + Math.pow(p2.y - p1.y, 2));
    };

    //判断是否碰到改边界
    public isCrash(point, radis) {
        var pointToLineLength = this.getPointToLineDistance(this.endPoint, this.startPoint, point);
        return pointToLineLength < radis + 10;
    }
    //获取点线的距离
    public getPointToLineDistance(p1, p2, p) {
        var ans = 0;
        var a = this.getPointDistance(p1, p2);
        var b = this.getPointDistance(p1, p);
        var c = this.getPointDistance(p2, p);
        if (c + b == a) {//点在线段上
            ans = 0;
            return ans;
        }
        if (a <= 0.00001) {//不是线段，是一个点
            ans = b;
            return ans;
        }
        if (c * c >= a * a + b * b) { //组成直角三角形或钝角三角形，p1为直角或钝角
            ans = b;
            return ans;
        }
        if (b * b >= a * a + c * c) {// 组成直角三角形或钝角三角形，p2为直角或钝角
            ans = c;
            return ans;
        }
        // 组成锐角三角形，则求三角形的高
        var p0 = (a + b + c) / 2;// 半周长
        var s = Math.sqrt(p0 * (p0 - a) * (p0 - b) * (p0 - c));// 海伦公式求面积
        ans = 2 * s / a;// 返回点到线的距离（利用三角形面积公式求高）
        return ans;
    }


    ///tbc
    public m_objType=objType.bound;
    public getObjType() {
		return this.m_objType;
	}


}