//因为考虑到地形可能是非正方形的，所以改变地形的因素。
//1 添加指定的边界，判断是否碰撞到边界


class BoundMgr {
	public constructor(gameScene) {
		this.m_GameScene = gameScene;

	}
	private m_GameScene;

	//我们来保存所有的线
	private LineList: BasicLine[] = [];

	//保存所有绘制的线的图像
	private shapList = [];
	//创建边界
	public checkBound(point, radis, crashObj = null) {
		
		//laa
		if (crashObj.myStates.state == TopState.shoot||crashObj.myStates.state == TopState.beCrash) {
			console.log("为发射或者becrash状态，边界不check");
			return false;
		}//laa
		
		for (let i = 0; i < this.LineList.length; i++) {
			if (this.LineList[i].isCrash(point, radis)) {

				//做碰撞函数
				this.LineList[i].beCrash(crashObj);
				return true;
			}

		}
		return false;
	}
	/**绘制出边界 */
	public DebugDrawBound(p1, p2) {
		if (serverConfig.config.debugDrawIsOpen) {
			var shp: egret.Shape = new egret.Shape();      //Shape是有绘图graphics功能的

			shp.graphics.lineStyle(5, 0xff0000);              // 5像素粗细， 颜色

			shp.graphics.moveTo(p1.x, p1.y);                          // 起始点的x,y坐标

			shp.graphics.lineTo(p2.x, p2.y);                        //终点x,y坐标

			shp.graphics.endFill();                                   //结束绘图


			this.m_GameScene.addChild(shp);

			return shp;
		}
	}
	// //添加边界
	// public AddBound(p1, p2, dataPoint) {
	// 	let tmpBound = new BasicLine(p1, p2, dataPoint);
	// 	this.DebugDrawBound(p1, p2);
	// 	this.LineList.push(tmpBound);
	// }
	//添加边界
	public AddBound(p1, p2, dataPoint) {
		let tmpBound = new BasicLine(p1, p2, dataPoint);
		let shp = this.DebugDrawBound(p1, p2);
		this.shapList.push(shp);
		this.LineList.push(tmpBound);
	}
	/**边界的移动*/
	public boundaryMove(speed) {
		let li = this.LineList;
		if (li[0].startPoint.x < serverConfig.config.minLineX) {
			let shp;
			for (let i = 0; i < li.length; i++) {
				this.m_GameScene.removeChild(this.shapList[i]);
				if (li[i].dataPoint.x == -1) {
					if (li[i].dataPoint.y == 0) {
						li[i].startPoint.y += speed;
						li[i].endPoint.y += speed;
						li[i].startPoint.x += speed;
						li[i].endPoint.x -= speed;
						// console.log("上");
					} else {
						li[i].startPoint.y -= speed;
						li[i].endPoint.y -= speed;
						li[i].startPoint.x += speed;
						li[i].endPoint.x -= speed;
						// console.log("下");
					}
				} else {
					if (li[i].dataPoint.x == 0) {
						li[i].startPoint.x += speed;
						li[i].endPoint.x += speed;
						li[i].startPoint.y += speed;
						li[i].endPoint.y -= speed;
						// console.log("右");
					} else {
						li[i].startPoint.y += speed;
						li[i].endPoint.y -= speed;
						li[i].startPoint.x -= speed;
						li[i].endPoint.x -= speed;
						// console.log("左");
					}
				}
				shp = this.DebugDrawBound(li[i].startPoint, li[i].endPoint);
				
				this.shapList[i] = shp;
			}
		}


	}
}