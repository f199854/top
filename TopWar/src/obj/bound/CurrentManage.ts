/**电流管理类 */
class CurrentManage extends eui.Component {
	/* 设置所有场景所在的舞台(根)*/
	private _stage: egret.DisplayObjectContainer;
	/**单例对象 */
	private static instance: CurrentManage;
	/**电流池子 */
	private electricityRess: egret.MovieClip[] = [];
	/**电流回收池子 */
	private electricityRecycleRess: egret.MovieClip[] = [];
	/**电流数量 */
	private electricityNum: number = 10;
	/**获取单例对象 */
	public static getInstance(): CurrentManage {
		if (this.instance == null) {
			this.instance = new CurrentManage();
		}
		return this.instance;
	}
	public constructor() {
		super();
		this.CreatElectricitys();
	}
	/**
	* 设置根场景
	* @param s 根场景
	*/
	public setStage(s: egret.DisplayObjectContainer) {
		this._stage = s;
	}
	/**创建电流对象 */
	private CreatElectricitys() {
		for (let i = 0; i < this.electricityNum; i++) {
			let electricityMc = FrameAnimation.getInstance().GetFrameAnimation("electricity", 0, 0, "electricity", -1);
			electricityMc.scaleX = electricityMc.skewY = 0.5;
			electricityMc.name = "0";
			this.electricityRess.push(electricityMc);
		}
	}
	/**获取电流 */
	private GetElectricity(): egret.MovieClip {
		if (this.electricityRess.length <= 0) {
			return;
		} else {
			for (let i = 0; i < this.electricityRess.length; i++) {
				if (this.electricityRess[i].name == "0") {
					this.electricityRess[i].name = "1";
					return this.electricityRess[i];
				}
			}
		}
	}
	/**绘制电网 */
	public DrawPowerGrid(p_1: Vector_2, p_2: Vector_2, p_3: Vector_2) {
		let pointRess: Vector_2[] = [p_2, p_3];
		for (let i = 0; i < pointRess.length; i++) {
			let p1 = p_1;
			let p2 = pointRess[i];
			let electricity_mc: egret.MovieClip = this.GetElectricity();
			if (electricity_mc == null) {
				return;
			}
			egret.Tween.removeTweens(electricity_mc);
			if (p1.x == p2.x) {//左右边界
				if (p1.x > 20) {
					electricity_mc.rotation = 90;
				} else {
					electricity_mc.rotation = -90;
				}
			}
			if (p1.y == p2.y) {//上下边界
				electricity_mc.rotation = 0;
				if (p1.y > 20) {
					electricity_mc.rotation = 180;
				}
			}
			electricity_mc.x = p1.x;
			electricity_mc.y = p1.y;
			this.electricityRecycleRess.push(electricity_mc);
			electricity_mc.visible = true;
			this._stage.addChild(electricity_mc);
			egret.Tween.get(electricity_mc)
				.to({ x: p2.x, y: p2.y }, 1000)
				.call(e => {
					electricity_mc.name = "0";
					electricity_mc.visible = false;
				})
		}

	}

}