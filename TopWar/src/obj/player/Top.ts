/**陀螺基类 */
class Top extends basicPlayer {
	//LYH记录当前坐标
	protected TopPoint = new egret.Point(0, 0);
	//protected dir = 0;



	/**VIEW---------------------------------- */
	/**陀螺内（圆） */
	protected top_Inner: eui.Image;
	/**陀螺外（刀刃） */
	protected top_outer: eui.Image;

	/**DATA---------------------------------- */

	/**半径 */
	protected radius: number;
	/**LYH出生位置坐标 */
	protected birtpoit = new egret.Point(0, 0);
	public isshow: boolean;
	public constructor() {
		super();
		this.isAI = false;
		super.init();
	}
	/**陀螺初始化 */
	public init() {
		this.dataInit();
		this.viewInit();
		this.isAI = false;
		this.nickName = "player";
		this.isshow = true;
		this.TopPoint=this.birtpoit;
	}
	/**视图初始化 */
	protected viewInit() {
		//外圈
		let knife = resManager.getInstance().addRes("top_knife1_png");
		this.top_outer = new eui.Image();
		this.top_outer.texture = knife;
		this.top_outer.width = this.top_outer.height = this.radius;
		this.top_outer.anchorOffsetX = this.top_outer.anchorOffsetY = this.top_outer.width / 2;
		this.addChild(this.top_outer);

		//内圈
		let head = resManager.getInstance().addRes("top_head1_1_png");
		this.top_Inner = new eui.Image();
		this.top_Inner.texture = head;
		this.top_Inner.width = this.top_Inner.height = Math.floor(this.radius / 1.59);
		this.top_Inner.anchorOffsetX = this.top_Inner.anchorOffsetY = this.top_Inner.width / 2;
		this.addChild(this.top_Inner);

		this.changeExpression(ExpressionState.normal);
	}

	/**数据初始化 */
	protected dataInit() {
		this.name = "";
		this.radius = serverConfig.config.TopInitSize;

	}


	/**显示更新 */
	public viewUpdate() {
		this.move();
		this.rotate();
		this.judgeIsPlayAnimation();
		//this.lowRotateSpeed();
		// if (this.lastpoit == this.ViewPoint) {
		// 	return;
		// }
		// else {
		// 	console.log("位置不一样");
		// }
	}

	/**减转速 */
	public lowRotateSpeed() {
		if (this.rotateSpeed <= 0) {
			this.rotateSpeed = 0;
			return;
		}
		//this.rotateSpeed -= this.deltaRotateSpeed;
	}
	/**LYH区域划分加速 */

	public SpeedUp(pold) {
		this.AimRotateSpeed = this.rotateSpeed;
		if (pold == Area.Gray) {
			//this.rotateSpeed += serverConfig.config.speedSize.SpeedGray;
			///**laa	新设变量 承接加速 */
			this.AimRotateSpeed += serverConfig.config.speedSize.SpeedGray;
		}
		if (pold == Area.Yellow) {
			//this.rotateSpeed += serverConfig.config.speedSize.SpeedYellow;
			this.AimRotateSpeed += serverConfig.config.speedSize.SpeedYellow;
		}
		if (pold == Area.Green) {
			//this.rotateSpeed += serverConfig.config.speedSize.SpeedGreen;
			this.AimRotateSpeed += serverConfig.config.speedSize.SpeedGreen;
		}
		if (pold == Area.Red) {
			//this.rotateSpeed += serverConfig.config.speedSize.SpeedRed;
			this.AimRotateSpeed += serverConfig.config.speedSize.SpeedRed;
		}
		if (this.rotateSpeed > serverConfig.config.speedSize.maxSpeedRotateSpeed) {
			//this.rotateSpeed = serverConfig.config.speedSize.maxSpeedRotateSpeed;
			this.AimRotateSpeed = serverConfig.config.speedSize.maxSpeedRotateSpeed;
			//this.bomb();
		}

		//	console.log(this.rotateSpeed);

	}




	//获取玩家摇杆的角度
	//每次摇杆事件更改摇杆方向
	public setRoleDir(dir) {
		this.dataTargetDir.setTo(dir.x, dir.y);
		//console.log("dir"+dir.x+".."+dir.y);
	}
	/**移动 */
	protected move() {
		//let poitC = this.lastpoit;
		this.ViewPoint.x = this.x;
		this.ViewPoint.y = this.y;
		let speed = this.speed / 6;
		if (this.ViewPoint.equals(this.dataPoint)) {
			return;
		}
		let distance = egret.Point.distance(this.ViewPoint, this.dataPoint);
		if (distance > speed) {
			this.ViewTargetPoint.setTo(this.dataPoint.x - this.x, this.dataPoint.y - this.y);
			this.ViewTargetPoint.normalize(speed);
			this.x += this.ViewTargetPoint.x;
			this.y += this.ViewTargetPoint.y;
		} else {
			this.x = this.dataPoint.x;
			this.y = this.dataPoint.y;
		}

		// if (this.myStates.state != TopState.shoot&&this.myStates.state!=TopState.beCrash) {
		// 	this.playerMoveDis();
		// }
		if (this.myStates.state == TopState.normal) {
			this.playerMoveDis();
		}



	}
	/**LYH计算走的距离 */
	public playerMoveDis() {
		
		if (this.TopPoint.x != this.ViewPoint.x || this.TopPoint.y != this.ViewPoint.y) {
			console.log("this.ViewPoint" + "  " + this.ViewPoint);
			console.log("this.TopPoint" + "  " + this.TopPoint)
			let dir = egret.Point.distance(this.ViewPoint, this.TopPoint);
			let su = dir*serverConfig.config.coeff;
			console.log("147852369" + "   " + dir);
			this.TopPoint.x = this.ViewPoint.x;
			this.TopPoint.y = this.ViewPoint.y;
			this.m_gameLogic.getGameScene().changeEnegry(su);
		}
	}
	/**转 */
	public rotate() {
		this.top_outer.rotation += this.rotateSpeed;

	}
	/**弹开 */
	protected flick() {

	}
	/**发射 */
	protected shoot() {

	}
	/**死亡 */
	public deathCallBack() {




		this.m_gameLogic.gameOver();
		SceneManager.instance.GetGameSceneObj().panelScene.DealSettlement();


	}
	public clear() {

	}
	/**重置 */
	public reset() {
		this.name = "";
		this.radius = 0;
	}
	/**销毁 */
	public destroy() {

	}
	/**LYH获取玩家出生位置 */
	public birth(x, y) {
		this.birtpoit.x = x;
		this.birtpoit.y = y;
		console.log(this.birtpoit);
	}
}
/**陀螺状态*/
enum TopState {
	normal, death, beCrash, shoot, bePk,speedstate
}

