class progressBarTs {
	public constructor() {
		this.dataInit();

	}
	//------VIEW--------------------------------
	/**操作面板场景 */
	private panelScene: PanelScene;

	/**速度 */
	public speed: number;

	/**获取能量条value值 */
	public getSpeed() {
		if (this.speed == null) {
			console.log("speed 为空");
			return null;
		}
		return this.speed;
	}

	/**设置能量条value值 */
	public setSpeed(num: number) {
		if (num == null) {
			console.log("传入value值为空");
			return null;
		}
		console.log("走了");
		this.speed = num;
		this.panelScene.progressBar.value = (num / serverConfig.config.maxSpeed) * 100;
		// this.panelScene.progressBar.value = num;
	}
	/**数据初始化 */
	private dataInit() {
		this.panelScene = SceneManager.instance.GetPanelSceneObj();
	}
	/**单例对象 */
	private static instance: progressBarTs;
	/**获取单例对象 */
	public static getInstance(): progressBarTs {
		if (this.instance == null) {
			this.instance = new progressBarTs();
		}
		return this.instance;
	}

}