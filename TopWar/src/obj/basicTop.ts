/**
 * 基础陀螺类
 * 1.完成陀螺的四种状态切换
 * 2.完成陀螺碰撞的逻辑
 * 
 */


class basicTop extends BasicObj {
	public constructor() {
		super();
		this.Init();
	}

	/**是否进入被撞状态 true为进入碰撞状态false为离开碰撞状态   tbc*/
	public isBeCrash: Boolean;
	/**设置碰撞对象 tbc */
	public beCrashObj: basicTop;
	/** tbc  记录陀螺更改状态前的状态  tbc 因为两个陀螺相撞第一个陀螺更改状态后 第二个陀螺判断碰撞到的陀螺状态已经被更改 
	  */
	public stateBeforeChange = {
		state: 0,
	};

	/**VIEW---------------------------------- */
	/**陀螺内（圆） */
	protected top_Inner: eui.Image;
	/**陀螺外（刀刃） */
	protected top_outer: eui.Image;
	/**半径 */
	protected radius: number;
	/**陀螺的碰撞范围与陀螺大小的比例 */
	protected ratio: number;

	/**血量 */
	public blood: number;

	/**陀螺大小所有的档位 */
	protected radiusGears = [100, 80, 60, 40, 30];//laa   200, 180, 140, 100, 60

	protected myRock;
	protected myLastStates;
	//防止撞击到同样的人
	protected LastCrashObj;

	/**动画显示对象 */
	protected armatureDisplay: dragonBones.EgretArmatureDisplay;

	/**显示名字的文本 */
	protected nameText: eui.Label;

	/**临时接收显示血量对象 */
	protected showBloodText: eui.Label;

	/**記錄下原來的尺寸 */
	protected nowBloodSize: Number;

	/*记录一下PK的对象 */

	protected PKObj: basicTop;
	/**能量 */
	protected energy: number;

	public isok: boolean = false;

	//当发生碰撞的时候.整个逻辑进行更改
	protected myStates =
	{
		state: 0,
		lastState: 0,
		statesStartTime: 0,

	};
	/**能量 */
	protected pkValue = 100;
	//初始化
	private Init() {
		this.data_init();
		this.uiInit();

	}
	/**数据初始化 */
	private data_init() {
		this.energy = 0;

		this.blood = 10000;

		this.ratio = this.myCrashRange / this.radius;
		this.rotateSpeed = 4;
	}



	/**设置PK的对象 */
	private setPKObj(tbasicTop: basicTop) {
		if (tbasicTop == null) {
			//	console.log("陀螺对象显示为空");
			return;
		}
		if (this.PKObj != null) {
			//	console.log("陀螺对象重复使用");
			return;
		}
		this.PKObj = tbasicTop;
	}

	//ui的初始化
	private uiInit() {

		this.addTopName();

		this.showBlood();
	}

	///laa
	public AimRotateSpeed = this.rotateSpeed;



	protected tmpPoint = new egret.Point();

	/**根據我們的需求，將轉速變成速度 */
	public changeRotataonSpeedToSpeed() {
		this.speed = this.rotateSpeed * 2;
	}

	///laa

	public isClickSpeedUp = false;
	public setIsClkSpdUp(bool) {
		this.isClickSpeedUp = bool;
	}

	/**计算移动 */
	public caculataMove() {

		// if (this.myStates.state == TopState.normal ) {
		// 	this.changeRotataonSpeedToSpeed();
		// 	if (this.isok == false) {
		// 		this.downRotateSpeed();
		// 	}

		// }
		//tbc  PK状态进行移动
		if (this.myStates.state == TopState.normal || this.myStates.state == TopState.bePk || this.myStates.state == TopState.speedstate) {
			this.changeRotataonSpeedToSpeed();
			if (this.isok == false) {
				this.downRotateSpeed();
			}

		}

		////**laa 点击加速 */
		if (this.isClickSpeedUp) {
			if (this.rotateSpeed < this.AimRotateSpeed) {
				this.addRotateSpeed();

			}
			else {
				this.isClickSpeedUp = false;
			}
		}


		this.dataTargetDir.normalize(this.speed);
		//w
		this.tmpPoint.setTo((this.dataPoint.x + this.dataTargetDir.x), (this.dataPoint.y + this.dataTargetDir.y));


		if (this.dataPoint == null) {
			console.log("移动一个不存在的陀螺");
			return;
		}


		///**laa */
		//console.log("x1y1x2y2");//20 20 1100 700
		///玩家在边界外
		if (this.x < serverConfig.config.boundSize.x1 || this.x > serverConfig.config.boundSize.x2 || this.y < serverConfig.config.boundSize.y1 || this.y > serverConfig.config.boundSize.y2) {
			this.dataPoint.x += this.dataTargetDir.x;
			this.dataPoint.y += this.dataTargetDir.y;
			return;
		}///laa

		//
		if (this.checkBound(this.tmpPoint, this) == false) {
			this.dataPoint.x += this.dataTargetDir.x;
			this.dataPoint.y += this.dataTargetDir.y;
		}
		else {
			if (this.dataPoint.x < (serverConfig.config.boundSize.x1 + this.myCrashRange)) {
				this.dataPoint.x = serverConfig.config.boundSize.x1 + this.myCrashRange;
			}
			else if (this.dataPoint.x > (serverConfig.config.boundSize.x2 - this.myCrashRange)) {
				this.dataPoint.x = serverConfig.config.boundSize.x2 - this.myCrashRange;
			}

			if (this.dataPoint.y < (serverConfig.config.boundSize.y1 + this.myCrashRange)) {
				this.dataPoint.y = serverConfig.config.boundSize.y1 + this.myCrashRange;
			}

			else if (this.dataPoint.y > (serverConfig.config.boundSize.y2 - this.myCrashRange)) {
				this.dataPoint.y = serverConfig.config.boundSize.y2 - this.myCrashRange;
			}

		}
	}

	public dataUpdate() {
		if (!this.isAI) {
			console.log("111" + "  " + this.myStates.state);
		}

		// if (this.states != TopState.normal)
		// 	return;

		////laa**界外掉血 */
		if (this.blood > 0) {
			if (this.x < serverConfig.config.boundSize.x1 || this.x > serverConfig.config.boundSize.x2 || this.y < serverConfig.config.boundSize.y1 || this.y > serverConfig.config.boundSize.y2) {
				this.changeBloodOut();
			}
		}

		if (this.isAI == true) {
			return;
		}
		// tbc  PK状态下可以移动
		// if (this.myStates.state != TopState.bePk) {
		// 	this.caculataMove();
		// }
		this.caculataMove();

		this.updateMyState()

		//tbc 更改离开碰撞后的属性
		this.leaveBeCrash();

	}
	/**加转速 */
	public addRotateSpeed() {
		//laa
		this.rotateSpeed += 0.1;
	}
	/**减转速 */
	public downRotateSpeed() {

		if (this.myStates.state == TopState.speedstate) {
			this.rageTimer();

		} else {
			this.rotateSpeed -= serverConfig.config.speedSize.SpeedDown;
		}

		if (this.rotateSpeed <= serverConfig.config.speedSize.minSpeedRotateSpeed)
			this.rotateSpeed = serverConfig.config.speedSize.minSpeedRotateSpeed;
	}
	/**LYH加大downspeed值 */
	public bomb() {
		this.rotateSpeed -= serverConfig.config.speedSize.SpeedDown * 20;
		// serverConfig.config.speedSize.SpeedDown = 0.6;
		//this.rotateSpeed = serverConfig.config.speedSize.minSpeedRotateSpeed;
	}


	//加载名字
	public addTopName() {
		this.nameText = new eui.Label();
		this.nameText.text = this.nickName;
		this.nameText.anchorOffsetX = this.nameText.width / 2;
		this.nameText.anchorOffsetY = this.nameText.height / 2;
		this.nameText.x = 0;
		this.nameText.y = -70;
		this.addChild(this.nameText);

	}

	/**LYH转速归零方法 */
	public rageTimer() {
		this.bomb();
	}
	private lastfaceStates;
	//根据速度来切换脸部表情
	public setSpeedFace(facestate) {

		if (facestate == this.lastfaceStates) {
			return;
		}
		this.lastfaceStates = facestate;

		this.changeExpression(facestate);

	}

	public getSpeedLevel() {
		return this.speedLevel;
	}
	public speedLevel = 0;
	public updateFace() {
		let num = serverConfig.config.speedRange.max;
		let verSpeed = serverConfig.config.speedSize.maxSpeedRotateSpeed / num + 1;
		if (this.rotateSpeed < verSpeed / 5) {
			this.speedLevel = ExpressionState.normal;
			this.setSpeedFace(ExpressionState.normal);
		}
		else if (this.rotateSpeed < verSpeed * 1.8) {
			this.speedLevel = ExpressionState.abnormal;
			this.setSpeedFace(ExpressionState.abnormal);
		}
		else if (this.rotateSpeed < verSpeed * 2.8) {
			this.speedLevel = ExpressionState.brag;
			this.setSpeedFace(ExpressionState.brag);
		}
		else if (this.rotateSpeed < verSpeed * 3) {
			this.speedLevel = ExpressionState.insane;
			this.setSpeedFace(ExpressionState.insane);
		}
		else {
			console.log("换取了错误的速度");
		}
	}

	//更改整个速度
	public changeSpeed(vstates, kp) {
		let steatsTmpTime = this.speed;
		switch (vstates) {
			case TopState.normal:

				break;

			case TopState.death:
				steatsTmpTime = 0
				break;
			case TopState.shoot:
				steatsTmpTime = 50
				break;

			case TopState.beCrash:
				steatsTmpTime = 30
				break;

			case TopState.bePk:
				steatsTmpTime = 0
				break;

			default:
				console.log("getStatesTime 出現問題");
				break;
		}
		return steatsTmpTime * kp;
	}

	public changeStates(states) {
		/**如果是碰撞狀態，則有可能出現被其他物體二次碰撞 */



		if (states != this.myLastStates) {

			this.changeMyState(states);

		}



	}

	/**获取玩家的摇杆
	 * 请用该函数获取摇杆，不要直接用变量，防止重复调用函数
	 * 
	 */
	protected getGameRock() {


		if (this.myRock == null) {


			this.myRock = this.m_gameLogic.getRocker();
		}
		return this.myRock;
	}


	/**获取状态 */
	public getMyStates() {
		return this.myStates;
	}



	/**判断陀螺碰撞AI的几种情况  第一个参数是自己陀螺的状态 第二个参数是碰撞的陀螺  tbc */
	public judgeMyTopState(mystate, crashObj) {
		if (mystate == null || crashObj == null) {
			console.log("判断陀螺碰撞情况参数为null" + "  mystate" + mystate + "  crashObj" + crashObj);
			return;
		}
		/**接收碰撞的情况  tbc */
		let collisionNum;
		switch (true) {
			case mystate == TopState.normal && crashObj.stateBeforeChange.state == TopState.normal:
				collisionNum = CollisionState.TwoNormal;

				break;
			case mystate == TopState.normal && crashObj.stateBeforeChange.state == TopState.shoot:
				collisionNum = CollisionState.normalAndShoot;

				break;
			case mystate == TopState.shoot && crashObj.stateBeforeChange.state == TopState.shoot:
				collisionNum = CollisionState.TwoShoot;

				break;
			case mystate == TopState.shoot && crashObj.stateBeforeChange.state == TopState.normal:
				collisionNum = CollisionState.shootAndNormal;

				break;
			case mystate == TopState.bePk && crashObj.stateBeforeChange.state == TopState.bePk:
				collisionNum = CollisionState.Pk;

				break;

		}
		return collisionNum;

	}

	/**处理碰撞状态  tbc */
	public handlesCollision(state, crashObj, Kp) {
		if (state == null || crashObj == null || Kp == null) {
			console.log("处理陀螺碰撞状态参数为null" + "  state" + state + "  crashObj" + crashObj + "  Kp" + Kp);
			return
		}

		let blood = 0;
		//控制反弹开关 
		let isRebound = true;
		/** tbc  判断撞到的物体是什么类型：
			 * 1.如果是Top的话进行分类讨论
			 * 2.如果是边界的话直接掉血
			*/
		if (crashObj.getObjType() == objType.top) {
			let collisionNum = this.judgeMyTopState(state, crashObj);
			//设置碰撞到的陀螺  tbc
			this.beCrashObj = crashObj;
			/**进入碰撞状态 tbc */
			this.isBeCrash = true;
			switch (collisionNum) {
				case CollisionState.TwoNormal:
					//如果双方都是normal状态的话进入PK 设置PK对象  在beCrash中根据Pk对象是否存在来判断是否走PK逻辑
					//同时避免了只碰撞一次而出现点击动画 （只有一直碰撞第二次调用碰撞函数的时候才会显示点击动画）
					this.setPKObj(crashObj);
					isRebound = false;
					break;
				case CollisionState.normalAndShoot:
					blood = (serverConfig.config.speedRange.max + 1) - this.getSpeedLevel();
					if (blood < 0)
						blood = 0;
					else
						blood = blood * 10;
					blood = Math.floor(blood);

					///laa  敌方shoot 更改反弹系数
					Kp = 4;//laa
					break;
				case CollisionState.TwoShoot:

					blood = this.getSpeedLevel() - crashObj.getSpeedLevel();
					if (blood < 0)
						blood = 0;
					else
						blood = blood * 10;

					///laa  敌方shoot 更改反弹系数
					Kp = 2;//laa

					break;
				case CollisionState.shootAndNormal:

					isRebound = false;

					break;
				case CollisionState.Pk:
					console.log("Pk掉血");

					break;
				default:
					console.log("反弹掉血");
					break;
			}
		} else {
			console.log("撞墙掉血");
			blood = 2;
		}

		//更改玩家的血量
		//	this.blood -= 10;
		console.log("myblood=" + blood);
		this.changeBlood(-1 * blood);
		// this.changeTopGrade();
		//	console.log("Kp=" + Kp);

		//碰撞后反弹  根据状态来判断是否要进行反弹
		//非空
		if (isRebound) {
			if (this.dataTargetDir == null) {
				console.log("basicTop 	this.dataTargetDir 是null")
				return null;
			}

			this.dataTargetDir.setTo(0, 0);
			let dir = this.calulatePopDir(this.dataPoint, crashObj.getDataPoint(this));

			//非空
			if (dir == null) {
				console.log("basicTop dir 为null");
				return null;
			}
			//	console.log("change user dir" + dir.x + "..." + dir.y);
			//下一帧的目标点
			this.dataTargetDir.setTo(dir.x, dir.y);

		}

		return Kp;
	}

	/**更改陀螺的状态 */
	//正常状态 摇杆可以接受信息，可以接受碰撞，
	//被碰撞状态，摇杆锁定，移动位置锁定，更改移动速度
	//放技能状态，摇杆锁定，接受碰撞，更改移动速度，
	//死亡状态，摇杆锁定，不接受碰撞

	//tbc
	public changeMyState(states, crashObj = null, shootdir = null, ) {


		/**是否反弹 true反弹 false不反弹  
		 * 
		 *1. 双方都是shoot的话则双方都弹飞
		 *2. 单方是shoot的话另一个被弹飞
		 *3. 都不是shoot的话就进入拼手速（显示点击动画）
		 * 
		 *  tbc*/


		// if (states == this.myStates.lastState)
		// 	return;

		//记录一下玩家的状态，防止被删除掉
		let mystate = this.myStates.state;

		//清除之前的所有状态，开始进行新的状态的绑定
		this.resetTopStates();

		// 在碰撞的时候记录碰撞之前的状态  在碰撞之前一直与自身状态一致 防止影响第一个运行碰撞的陀螺 tbc
		this.stateBeforeChange.state = states;

		//只有玩家的陀螺，才会有父场景选项      
		//tbc  多加了一个是否处于PK状态属性   
		if (this.m_gameLogic != null && this.isAI == false && states != TopState.normal) {
			//进行锁杆
			this.getGameRock().setLock(true)
		}
		let Kp = 1;

		switch (states) {
			case TopState.speedstate:
				//LYH减速状态下摇杆锁定

				this.getGameRock().setLock(true);
				//**laa   锁杆  下一帧位置归零*/
				this.dataTargetDir.setTo(0, 0);
				break;
			case TopState.normal:
				this.dataTargetDir.setTo(0, 0);

				break;
			case TopState.bePk:
				//PK时候不进行锁杆  解杆   laa
				if (!this.isAI) {
					this.getGameRock().setLock(false);
				}
				let gamescene = this.m_gameLogic.getGameScene();
				//只有碰到了玩家才會彈出
				if (!this.isAI) {
					gamescene.isShowClick(true);
				}
				Kp = this.handlesCollision(TopState.bePk, crashObj, Kp);

				/***laa 碰撞减速 */

				if (this.rotateSpeed > 0) {
					this.rotateSpeed -= serverConfig.config.speedSize.crashSpeedLose;

				}
				break;
			case TopState.shoot:
				this.dataTargetDir.setTo(0, 0);
				this.dataTargetDir.setTo(shootdir.x, shootdir.y);
				break;
			case TopState.beCrash:
				/***laa 碰撞减速 */
				this.rotateSpeed -= serverConfig.config.speedSize.crashSpeedLose;
				if (this.rotateSpeed < 0) {
					this.rotateSpeed = 0;
				}
				//记录碰撞之前的状态 tbc
				this.stateBeforeChange.state = mystate;

				Kp = this.handlesCollision(mystate, crashObj, Kp);

				break;
			case TopState.death:


				console.log(this.nickName + "更改為死亡狀態");
				break;
			default:
				console.log("未找到该类型");
				break;
		}

		this.setStates(states, Kp);

	}



	/**更改表情 */
	public changeExpression(state: ExpressionState) {
		this.frogAnimationInit();
		this.top_Inner.visible = false;
		let image: egret.Texture;
		switch (state) {
			case ExpressionState.normal:
				image = resManager.getInstance().addRes("BlueTimid_png");
				this.top_Inner.texture = image;
				this.top_Inner.visible = true;
				break;
			case ExpressionState.abnormal:
				//this.armatureDisplay = meterAanimation.getInstance().GetDragonAnimation(ExpressionState.abnormal);
				// this.armatureDisplay.x = this.top_Inner.x;
				// this.armatureDisplay.y = this.top_Inner.y;
				// this.addChild(this.armatureDisplay);
				//LYH替换努力表情资源
				image = resManager.getInstance().addRes("BlueStrive_png");
				this.top_Inner.texture = image;
				this.top_Inner.visible = true;
				break;
			case ExpressionState.brag:
				this.armatureDisplay = meterAanimation.getInstance().GetDragonAnimation(ExpressionState.brag);
				this.armatureDisplay.x = this.top_Inner.x;
				this.armatureDisplay.y = this.top_Inner.y;
				this.addChild(this.armatureDisplay);
				break;
			case ExpressionState.insane:
				image = resManager.getInstance().addRes("BlueRage_png");
				this.top_Inner.texture = image;
				this.top_Inner.visible = true;
				break;
			default:
				break;

		}
	}
	public changePkValue(num) {
		if (this.myStates.state == TopState.death) {
			console.log("该玩家已经死亡")
			return;
		}
		this.pkValue += num;
		//	this.showBloodText.text = this.pkValue + "";
	}
	public changeBlood(num) {

		if (this.myStates.state == TopState.death) {
			console.log("该玩家已经死亡")
			return;
		}


		this.blood += num;

		if (this.blood <= 0) {
			this.blood = 0;
			this.death();
		}
		this.changeBloodShow();
	}
	public isDeath() {
		return this.myStates.state == TopState.death;
	}
	public getPkValue() {
		return this.pkValue;
	}

	/**用于PK结束，更改状态，同时关闭点击界面 */
	public PKOverCallback(isDeath) {
		this.pkValue
	}

	/**设置状态 */
	public setStates(states, kp = 1) {
		if (states == TopState.normal)
			return;
		this.speed = this.changeSpeed(states, kp);
		this.myStates.lastState = states;
		this.myStates.state = states;
		this.myStates.statesStartTime = Date.now();
	}

	//发生了碰撞
	//1.陀螺，根据两个物体的转速，进行减速计算，更改碰撞状态，计算碰撞后的朝向，
	//2、道具则自我计算
	public beCrash(crashObj) {
		//根据是否存在PK对象来判断是否要走PK逻辑  tbc
		if (this.PKObj != null) {
			this.bePk(crashObj);
			return;
		}

		if (this.blood > 0) {
			//	console.log("发生碰撞喽~~掉血了");


			this.changeMyState(TopState.beCrash, crashObj);
			console.log("碰撞" + crashObj.nickName + "blood=" + this.blood);
			if (!this.isAI) {
				//this.m_gameLogic.getGameScene().changeEnegry(30);
			}

			//	this.states = TopState.beCrash;
		}
	}

	//判断
	public bePk(crashObj) {
		if (this.myStates.state == TopState.death) {
			console.log("不要鞭尸");
			return;
		}
		// this.setPKObj(crashObj);
		this.changeMyState(TopState.bePk, crashObj);
		//console.log("开始PK" + crashObj.nickName + "blood=" + this.blood);

	}
	/**更改陀螺当前所在等级区域 */
	public changeTopGrade() {
		//	this.showBloodText.text = this.blood + "";
		/**陀螺在哪个等级 */
		let grade = 0;
		if (this.blood >= 80) {
			grade = 0;
		} else if (this.blood >= 60) {
			grade = 1;
		} else if (this.blood >= 40) {
			grade = 2;
		} else if (this.blood >= 20) {
			grade = 3;
		} else if (this.blood > 0) {
			grade = 4;
		} else {
			console.log("陀螺死亡");

			return;
		}

		if (this.nowBloodSize != grade) {
			console.log("改變尺寸" + grade);
			this.changeTopSize(grade);
			this.nowBloodSize = grade;
		}

	}

	/**改变陀螺大小 */
	public changeTopSize(grade) {
		// //改变陀螺大小
		var tw = TweenManger.getInstance().getTweenObject(this, false);
		tw.to({ scaleX: this.radiusGears[grade] / serverConfig.config.TopInitSize, scaleY: this.radiusGears[grade] / serverConfig.config.TopInitSize }, 100);

		this.radius = this.radiusGears[grade];
		this.myCrashRange = this.radius * this.ratio;
		//改变名字的位置
		this.nameText.y += 10;
	}

	/**设置碰撞 */
	public setCrash(point) {

	}


	/**清空陀螺的状态 */
	public resetTopStates() {
		//tbc 
		this.stateBeforeChange.state = TopState.normal;

		this.myStates.state = TopState.normal;
		this.myStates.statesStartTime = -1;
		this.myStates.lastState = TopState.normal;
		this.speed = serverConfig.config.speedSize.minSpeed;

		if (!this.isAI) {
			let gamescene = this.m_gameLogic.getGameScene();
			gamescene.sendPKOrder(ShootControlState.normal);
			this.getGameRock().setLock(false);/////**laa */
		}


	}
	/*获取更改状态的时间*/

	public getStatesTime(state) {
		let steatsTmpTime = 0
		switch (state) {
			case TopState.normal:
				steatsTmpTime = 0
				break;

			case TopState.death:
				steatsTmpTime = 1000
				break;
			case TopState.shoot:
				steatsTmpTime = 600
				break;

			case TopState.beCrash:
				steatsTmpTime = 100
				break;
			case TopState.bePk:
				steatsTmpTime = 200000
				break;

			default:
				console.log("getStatesTime 出現問題");
				break;
		}
		return steatsTmpTime;
	}

	/*用来检验点是否在范围之外*/
	protected checkBound(point, crashobj = null) {
		return this.m_gameLogic.getGameScene().checkCraskBound(point, this.myCrashRange, crashobj);
	}


	/**更改本人的狀態 */
	public updateMyState() {




	}

	/**添加动画 */
	public addAnimator(num: number) {
		let meterAni = meterAanimation.getInstance();
		this.armatureDisplay = meterAni.GetDragonAnimation(num);
		this.addChild(this.armatureDisplay);
		meterAni.openClock();
		this.armatureDisplay.visible = false;
	}
	/**动画初始化 */
	protected frogAnimationInit() {
		if (this.armatureDisplay) {
			this.removeChild(this.armatureDisplay);
			this.armatureDisplay.armature.dispose();
			this.armatureDisplay = null;
		}
	}
	/**显示血量临时使用 */
	public showBlood() {
		// this.showBloodText = new eui.Label();
		// this.showBloodText.text = this.blood + "";
		// this.showBloodText.anchorOffsetX = this.showBloodText.width / 2;
		// this.showBloodText.anchorOffsetY = this.showBloodText.height / 2;
		// this.showBloodText.x = 0;
		// this.showBloodText.y = 70;
		// this.addChild(this.showBloodText);
		//this.Ming.Blood.text = this.blood+"";

	}
	public changeBloodShow() {
		// this.showBloodText.text = this.blood + "";
		//this.Ming.Blood.text = this.blood+"";
	}

	/**显示更新 */
	public viewUpdate() {

		this.judgeIsPlayAnimation();

	}

	/**判断是否播放动画 */
	public judgeIsPlayAnimation() {
		return;
		if (this.rotateSpeed / serverConfig.config.maxSpeed >= 0.5) {
			this.armatureDisplay.visible = true;
		} else {
			this.armatureDisplay.visible = false;
		}
	}

	/**计算距离 */
	public calculateDistance(obj) {
		if (obj != null) {
			var x = Math.abs(this.x - obj.x);
			var y = Math.abs(this.y - obj.y);
			return Math.sqrt(x * x + y * y);
		}
	}
	/**获取陀螺半径*/
	public getTopRadius() {
		return this.radius;
	}

	/**离开碰撞状态
	 * 如果玩家与碰撞到的陀螺距离大于两陀螺半径之和就离开碰撞状态  tbc
	 */
	public leaveBeCrash() {
		//碰撞物体不为空
		if (this.beCrashObj != null) {
			console.log("this.beCrashObj.getMyStates().state" + " " + this.beCrashObj.getMyStates().state);
			//如果碰撞物体死亡或者与碰撞物体距离超过一定距离  为了与触发碰撞函数的距离接近所以乘以 0.3 的系数
			//第一个条件是碰撞对象死亡后碰撞不到陀螺，玩家恢复normal状态
			if (this.calculateDistance(this.beCrashObj) > 0.3 * (this.radius + this.beCrashObj.getTopRadius())) {
				this.stateBeforeChange.state = TopState.normal;
				this.isBeCrash = false;
				this.beCrashObj = null;
				//如果PK对象不为空的话就隐藏点击动画
				if (this.PKObj != null) {
					this.PKObj = null;
					let gamescene = this.m_gameLogic.getGameScene();
					gamescene.isShowClick(false);
					//离开PK清空陀螺状态
					this.resetTopStates();
				}


			}
		}

	}

	///laa/
	/**
	 * 界外掉血
	 */
	public changeBloodOut() {

		if (this.myStates.state == TopState.death) {
			console.log("该玩家已经死亡")
			return;
		}
		//根据配置文件界外掉血参数  掉血
		this.blood -= serverConfig.config.BloodLoseOut;

		if (this.blood <= 0) {
			this.blood = 0;
			this.death();

		}
		this.changeBloodShow();
	}

}
/**表情状态 */
enum ExpressionState {
	normal, abnormal, brag, insane, spellHandSpeed
}

/**碰撞状态  tbc
 * TwoNormal:normal normal
 * normalAndShoot: normal shoot
 * TwoShoot:shoot  shoot
 * shootAndNormal:shoot  normal
 * Pk:pk  pk
 */
enum CollisionState {
	TwoNormal, normalAndShoot, TwoShoot, shootAndNormal, Pk
}