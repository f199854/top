/** 
 * 物理世界
 * 
 * 1、处理所有在世界的物体的碰撞
 * 2、当物体到达边界的时候 触发相应的事件
 * 3. 接受update事件，逻辑帧，为1秒钟15帧
*/

class phyWorld {

	private topId;
	/**本地地图上所有陀螺 */
	private topList: BasicObj[] = [];

	public isUsed;

	/**判断整个地图的边界 */
	public mapRangeSize = 0;


	private m_gameLogic: gameLogic;

	//记录整个游戏的原点
	public zeroPoint = new egret.Point(1250, 1250);



	public constructor() {

	}

	/**获取所有的tops */
	public getTops() {
		return this.topList;
	}
	public init(gamelogic) {
		this.m_gameLogic = gamelogic;
		this.isUsed = true;
		this.topId = 0;
		this.mapRangeSize = serverConfig.config.mapMaxRangeLength;
	}
	public addObjInWorld(top: BasicObj) {
		this.topId++;
		top.settopId(this.topId);
		this.topList.push(top);
		return this.topId;
	}

	/**将其从物体中删除 */
	public removeObj(basicobj) {
		Util.getInstance().removeItemInPool(this.topList, basicobj.id);
	}
	public clean() {
		let top: BasicObj = null;
		for (top of this.topList) {
			top.reset();
			top = null;

		}
		this.topList = [];
		this.topId = 0;
	}
	public viewUpdate() {
		if (this.isUsed == false)
			return;
		//开始移动
		let top: BasicObj = null;
		for (top of this.topList) {
			if (!top.isDeath()) {
				top.viewUpdate();
			}


		}


	}
	public update(gamesceneTmp) {
		if (this.isUsed == false)
			return;
		//开始移动
		let toptmp = this.m_gameLogic.getMyTop();
		let top: BasicObj = null;
		let dirtmp = gamesceneTmp.GetMyTopDir();
		if (dirtmp != null) {
			toptmp.setRoleDir(dirtmp);
		}
		gamesceneTmp.progressBarDisplay(toptmp.getRotateSpeed());


		for (top of this.topList) {
			if (!top.isDeath())
				top.dataUpdate();
		}

		//开始判断距离
		// this.checkIsDeath();


		// //物体的碰撞
		// //当物体碰撞后，计算碰撞的角度，将碰撞的物体信息传给被碰的物体，为了保证该信息不会因为死亡而消融，所以需要
		for (top of this.topList) {
			if (!top.isDeath())
				this.checkCrash(top);
		}

		// // //开始判断距离
		//this.checkIsDeath();

	}



	//找到跟玩家最近的碰撞玩家，弹开
	// public checkCrash(obj) {
	// 	let top;
	// 	let minCrashTop = null;
	// 	let minUserDistance = 10000;
	// 	let nowDistance;
	// 	let minSize;
	// 	let tmpState = obj.getMyStates();
	// 	if (tmpState != null && tmpState.state == TopState.bePk) {
	// 		console.log("处于飞升状态，无敌");
	// 		return;
	// 	}

	// 	if (tmpState == TopState.death) {
	// 		console.log("玩家死亡")
	// 		return;
	// 	}

	// 	for (let i = 0; i < this.topList.length; i++) {
	// 		top = this.topList[i];
	// 		if (top.gettopId() == obj.gettopId())
	// 			continue;
	// 		if (top.getMyStates().state == TopState.bePk)
	// 			continue;

	// 		minSize = obj.getMyCrashRange() + top.getMyCrashRange();
	// 		nowDistance = Util.getInstance().getDistance(obj.getDataPoint(), top.getDataPoint());
	// 		//如果彼此距离大于碰撞距离，则触发碰撞事件
	// 		if (nowDistance < minSize) {

	// 			if (nowDistance < minUserDistance) {
	// 				minUserDistance = nowDistance;
	// 				minCrashTop = this.topList[i];
	// 				continue;

	// 			}


	// 		}


	// 	}

	// 	//触发碰撞的函数
	// 	if (minCrashTop != null) {


	// 		if (!this.IsPlayPk(obj, minCrashTop)) {
	// 			obj.beCrash(minCrashTop);
	// 		}

	// 	}


	// }

	//tbc
	public checkCrash(obj) {
		let top;
		let minCrashTop = null;
		let minUserDistance = 10000;
		let nowDistance;
		let minSize;
		let tmpState = obj.getMyStates();
		// if (tmpState != null && tmpState.state == TopState.bePk) {
		// 	console.log("处于飞升状态，无敌");
		// 	return;
		// }

		if (tmpState == TopState.death) {
			console.log("玩家死亡")
			return;
		}

		for (let i = 0; i < this.topList.length; i++) {
			top = this.topList[i];
			if (top.gettopId() == obj.gettopId())
				continue;
			// if (top.getMyStates().state == TopState.bePk)
			// 	continue;

			minSize = obj.getMyCrashRange() + top.getMyCrashRange();
			nowDistance = Util.getInstance().getDistance(obj.getDataPoint(), top.getDataPoint());
			//如果彼此距离大于碰撞距离，则触发碰撞事件
			if (nowDistance < minSize) {

				if (nowDistance < minUserDistance) {
					minUserDistance = nowDistance;
					minCrashTop = this.topList[i];
					continue;

				}


			}


		}

		//触发碰撞的函数  
		if (minCrashTop != null) {
			//tbc 去掉最早的PK逻辑
			obj.beCrash(minCrashTop);

		}

	}



	//如果两个陀螺都没有发射，则自动进入PK状态
	public IsPlayPk(top1, top2) {

		//必须都为陀螺
		if (top1.getObjType() != objType.top || top2.getObjType() != objType.top) {
			return false;

		}
		//判断一下状态没有问题
		if (top1.getMyStates() != TopState.shoot && top2.getMyStates() != TopState.shoot && top2.getSpeedLevel() == top1.getSpeedLevel()) {

			//	let t = GameUtil.getRandomByWeight([20, 80]);
			let t = 0;
			if (t == 0) {
				top1.bePk(top2);
				top2.bePk(top1);
				return true;
			}
			else {
				return false;
			}

		}

		//进入最强PK状态
		return false;
	}
	public getRetainPlayerNum() {
		return this.topList.length;
	}
	/**判断物体是否碰线死亡*/
	public checkIsDeath() {
		let top;

		for (let tu = (this.topList.length - 1); tu >= 0; tu--) {
			top = this.topList[tu];
			if (Util.getInstance().getDistance(top.getDataPoint(), this.zeroPoint) > this.mapRangeSize) {
				this.topList.splice(this.findObjIndex(this.topList, top.topId), 1);
				//直接调用死亡事件
				top.death();

			}
		}

	}

	/**获取物体的位置和索引 */
	public findObjIndex(arr, topId) {
		var len = arr.length;

		while (len--) {
			if (arr[len].topId === topId) {
				return len;
			}
		}

		return -1;
	}

}